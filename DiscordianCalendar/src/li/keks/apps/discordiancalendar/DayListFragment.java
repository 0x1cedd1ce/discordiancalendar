package li.keks.apps.discordiancalendar;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CalendarContract;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import li.keks.apps.discordiancalendar.calendar.DiscordianDay;

/**
 * A fragment representing a single Season detail screen. This fragment is
 * either contained in a {@link SeasonListActivity} in two-pane mode (on
 * tablets) or a {@link DayListActivity} on handsets.
 */
public class DayListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>,
        GestureOverlayView.OnGesturePerformedListener, View.OnClickListener,
        View.OnLongClickListener, View.OnTouchListener, CalendarObserverCallback {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";
    /**
     * The dummy content this fragment is presenting.
     */
    private DiscordianDay mDay;
    /**
     * the header layout
     */
    private LinearLayout m_headerLayout;
    /**
     * the header text view
     */
    private TextView m_headerTextView;
    /**
     * the list view
     */
    private RelativeLayout m_dayList;
    /**
     * uri for the loader
     */
    private Uri m_LoaderUri;
    /**
     * Library with gestures
     */
    private GestureLibrary gestureLib;
    /**
     * content observer
     */
    private CalendarObserver mContentObserver;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DayListFragment() {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            this.onChange();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            DiscordianDay mDay = (DiscordianDay) getArguments().getSerializable(ARG_ITEM_ID);
            assert mDay != null;
            setDiscordianDay(mDay);
        }
    }

    public void setDiscordianDay(DiscordianDay Day) {
        this.mDay = Day;
        GregorianCalendar beginTime = mDay.getGregorianDay();
        GregorianCalendar endTime = mDay.getGregorianDay();

        beginTime.set(Calendar.HOUR_OF_DAY, 0);
        beginTime.set(Calendar.MINUTE, 1);
        long startMillis = beginTime.getTimeInMillis();

        endTime.set(Calendar.HOUR_OF_DAY, 23);
        endTime.set(Calendar.MINUTE, 59);
        long endMillis = endTime.getTimeInMillis();

        Uri.Builder builder = CalendarContract.Instances.CONTENT_URI.buildUpon();
        ContentUris.appendId(builder, startMillis);
        ContentUris.appendId(builder, endMillis);

        this.m_LoaderUri = builder.build();
        getActivity().getActionBar().setTitle(mDay.toString());
        getLoaderManager().restartLoader(1, null, this);
        String sHolyDay = mDay.getHolyDay();
        try {
            m_headerTextView.setText(sHolyDay);
        } catch (NullPointerException e) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        GestureOverlayView rootView = (GestureOverlayView) inflater.inflate(R.layout.fragment_day_list,
                container, false);

        // Show the dummy content as text in a TextView.
        if (mDay != null) {
            assert rootView != null;
            m_headerTextView = ((TextView) rootView.findViewById(R.id.headerTextView));
            m_headerTextView.setOnClickListener(this);
            m_dayList = ((RelativeLayout) rootView.findViewById(R.id.dayList));
            getActivity().getActionBar().setTitle(mDay.toString());
            if (mContentObserver == null) {
                mContentObserver = new CalendarObserver(new Handler(), this);
                getActivity().getContentResolver().registerContentObserver(CalendarContract.CONTENT_URI, true, mContentObserver);
            }
            getLoaderManager().restartLoader(1, null, this);
            String sHolyDay = mDay.getHolyDay();
            m_headerTextView.setText(sHolyDay);
        }
        //m_dayList.setOnLongClickListener(this);
        for (int i = 0; i < m_dayList.getChildCount(); i++) {
            if (m_dayList.getChildAt(i) instanceof TextView &&
                    m_dayList.getChildAt(i).getTag() != null) {
                m_dayList.getChildAt(i).setOnLongClickListener(this);
                m_dayList.getChildAt(i).setOnClickListener(this);
                m_dayList.getChildAt(i).setOnTouchListener(this);
            }
        }

        rootView.addOnGesturePerformedListener(this);
        gestureLib = GestureLibraries.fromRawResource(this.getActivity(), R.raw.gestures);
        if (!gestureLib.load()) {
            this.getActivity().finish();
        }

        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mContentObserver != null) {
            getActivity().getContentResolver().unregisterContentObserver(mContentObserver);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mContentObserver != null) {
            getActivity().getContentResolver().registerContentObserver(CalendarContract.CONTENT_URI, true, mContentObserver);
        }
    }

    /**
     * Instantiate and return a new Loader for the given ID.
     *
     * @param id   The ID whose loader is to be created.
     * @param args Any arguments supplied by the caller.
     * @return Return a new Loader instance that is ready to start loading.
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this.getActivity(), this.m_LoaderUri, Appointment.projection,
                Appointment.selection, Appointment.selection_arguments, Appointment.arguments);
    }

    /**
     * Called when a previously created loader has finished its load.  Note
     * that normally an application is <em>not</em> allowed to commit fragment
     * transactions while in this call, since it can happen after an
     * activity's state is saved.  See { FragmentManager#beginTransaction()
     * FragmentManager.openTransaction()} for further discussion on this.
     * <p/>
     * <p>This function is guaranteed to be called prior to the release of
     * the last data that was supplied for this Loader.  At this point
     * you should remove all use of the old data (since it will be released
     * soon), but should not do your own release of the data since its Loader
     * owns it and will take care of that.  The Loader will take care of
     * management of its data so you don't have to.  In particular:
     * <p/>
     * <ul>
     * <li> <p>The Loader will monitor for changes to the data, and report
     * them to you through new calls here.  You should not monitor the
     * data yourself.  For example, if the data is a {@link android.database.Cursor}
     * and you place it in a {@link android.widget.CursorAdapter}, use
     * the {@link android.widget.CursorAdapter#CursorAdapter(android.content.Context,
     * android.database.Cursor, int)} constructor <em>without</em> passing
     * in either {@link android.widget.CursorAdapter#FLAG_AUTO_REQUERY}
     * or {@link android.widget.CursorAdapter#FLAG_REGISTER_CONTENT_OBSERVER}
     * (that is, use 0 for the flags argument).  This prevents the CursorAdapter
     * from doing its own observing of the Cursor, which is not needed since
     * when a change happens you will get a new Cursor throw another call
     * here.
     * <li> The Loader will release the data once it knows the application
     * is no longer using it.  For example, if the data is
     * a {@link android.database.Cursor} from a {@link android.content.CursorLoader},
     * you should not call close() on it yourself.  If the Cursor is being placed in a
     * {@link android.widget.CursorAdapter}, you should use the
     * {@link android.widget.CursorAdapter#swapCursor(android.database.Cursor)}
     * method so that the old Cursor is not closed.
     * </ul>
     *
     * @param loader The Loader that has finished.
     * @param data   The data generated by the Loader.
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        ArrayList<Appointment> lAppointments = new ArrayList<Appointment>();
        while (data.moveToNext()) {
            Appointment app = new Appointment(data);
            if (app.compareTo(mDay) == 0) {
                lAppointments.add(app);
            }
        }
        lAppointments.trimToSize();
        Collections.sort(lAppointments);
        updateAppointments(lAppointments);
    }

    private void updateAppointments(List<Appointment> lAppointments) {
        for (int i = m_dayList.getChildCount() - 1; i >= 0; i--) {
            if (m_dayList.getChildAt(i).getTag() == "appointment") {
                m_dayList.removeViewAt(i);
                i = m_dayList.getChildCount();
            }
        }
        int heightMulti = m_dayList.findViewById(R.id.i0).getMinimumHeight();
        LinkedList<Pair<Integer, ViewGroup>>[] views = new LinkedList[24];
        Point p = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(p);
        int width = p.x - m_dayList.findViewById(R.id.i0).getWidth();
        int curId = R.id.i23;
        for (int i = 0; i < 24; i++) {
            views[i] = new LinkedList<Pair<Integer, ViewGroup>>();
        }
        for (Appointment app : lAppointments) {
            int beginHour = app.getStartDay().getHour();
            double beginMinute = app.getStartDay().getMinute();
            if (app.getStartDay().getDay() < this.mDay.getDay() || app.getAllDay()) {
                beginHour = 0;
                beginMinute = 0;
            }
            int endHour = app.getEndDay().getHour();
            double endMinute = app.getEndDay().getMinute();
            if (app.getEndDay().getDay() > this.mDay.getDay() || app.getAllDay()) {
                endHour = 24;
                endMinute = 0;
            }
            LinearLayout layout = new LinearLayout(getActivity());
            layout.setOnLongClickListener(this);
            layout.setOnTouchListener(this);
            TextView view = new TextView(this.getActivity());
            view.setText(app.getTitle());
            view.setBackgroundColor(app.getColor());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            layout.addView(view, layoutParams);
            curId++;
            layout.setId(curId);
            layout.setTag("appointment");
            layout.setTag(R.id.appointment_id, app);
            int pos = 0;
            int leftId = m_dayList.getChildAt(beginHour).getId();
            for (Pair<Integer, ViewGroup> i : views[beginHour]) {
                if (i.first == pos) {
                    leftId = i.second.getId();
                    pos++;
                } else {
                    break;
                }
            }
            double topMargin = heightMulti * (beginMinute / 60.0);
            double length = (endHour - beginHour) + endMinute / 60 - beginMinute / 60;
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    width - 4, (int) (length * heightMulti));
            params.addRule(RelativeLayout.RIGHT_OF, leftId);
            params.addRule(RelativeLayout.ALIGN_TOP, m_dayList.getChildAt(beginHour).getId());
            params.setMargins(2, 2, 2, 2);
            layout.setPadding(0, (int) topMargin, 0, 0);
            m_dayList.addView(layout, params);
            Pair<Integer, ViewGroup> pair = new Pair<Integer, ViewGroup>(pos, layout);
            for (int i = beginHour; i < endHour && i < 24; i++) {
                int j = 0;
                for (; j < views[i].size(); j++) {
                    if (views[i].get(j).first > pos) {
                        break;
                    }
                }
                views[i].add(j, pair);
            }
        }
        for (int i = 0; i < 24; i++) {
            if (views[i].size() != 0) {
                int new_width = width / views[i].size() - 4;
                for (Pair<Integer, ViewGroup> pair : views[i]) {
                    RelativeLayout.LayoutParams params =
                            (RelativeLayout.LayoutParams) pair.second.getLayoutParams();
                    if (params.width > new_width) {
                        params.width = new_width;
                        pair.second.setLayoutParams(params);
                    }
                }
            }
        }
        for (int i = 23; i >= 0; i--) {
            if (views[i].size() != 0) {
                int min_width = views[i].get(0).second.getLayoutParams().width;
                for (Pair<Integer, ViewGroup> pair : views[i]) {
                    RelativeLayout.LayoutParams params =
                            (RelativeLayout.LayoutParams) pair.second.getLayoutParams();
                    if (params.width < min_width) {
                        min_width = params.width;
                    }
                }
                for (Pair<Integer, ViewGroup> pair : views[i]) {
                    RelativeLayout.LayoutParams params =
                            (RelativeLayout.LayoutParams) pair.second.getLayoutParams();
                    params.width = min_width;
                    pair.second.setLayoutParams(params);
                }
            }
        }
    }

    /**
     * Called when a previously created loader is being reset, and thus
     * making its data unavailable.  The application should at this point
     * remove any references it has to the Loader's data.
     *
     * @param loader The Loader that is being reset.
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public void onGesturePerformed(GestureOverlayView gestureOverlayView, Gesture gesture) {
        if (gesture.getLength() > 300) {
            ArrayList<Prediction> predictions = gestureLib.recognize(gesture);
            for (Prediction prediction : predictions) {
                if (prediction.score > 1.0) {
                    if (prediction.name.equals("next")) {
                        setDiscordianDay(this.mDay.inc());
                    } else if (prediction.name.equals("prev")) {
                        setDiscordianDay(this.mDay.dec());
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == this.m_headerTextView.getId()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(mDay.getHolyDayDesc());
            builder.setTitle(mDay.getHolyDay());
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    @Override
    public boolean onLongClick(View view) {
        if (view instanceof LinearLayout && view.getTag() != null && view.getTag().equals("appointment")) {
            Appointment app = (Appointment) view.getTag(R.id.appointment_id);
            Intent eventIntent = new Intent(this.getActivity(), NewEventActivity.class);
            eventIntent.putExtra(NewEventFragment.ARG_APP, app);
            startActivityForResult(eventIntent, 0);
            return true;
        }
        if (view instanceof TextView && view.getTag() != null && view.getTag() instanceof String) {
            String t = (String) view.getTag();
            Intent eventIntent = new Intent(this.getActivity(), NewEventActivity.class);
            eventIntent.putExtra(NewEventFragment.ARG_DAY, this.mDay);
            eventIntent.putExtra(NewEventFragment.ARG_HOUR, t);
            startActivityForResult(eventIntent, 0);
            return true;
        }
        return false;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view instanceof TextView) {
            if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                view.setBackgroundColor(Color.BLUE);
            }
            if (motionEvent.getActionMasked() == MotionEvent.ACTION_UP ||
                    motionEvent.getActionMasked() == MotionEvent.ACTION_OUTSIDE ||
                    motionEvent.getActionMasked() == MotionEvent.ACTION_CANCEL ||
                    motionEvent.getActionMasked() == MotionEvent.ACTION_POINTER_UP) {
                view.setBackgroundResource(R.drawable.bottom_border);
            }
        }
        return false;
    }

    @Override
    public void onChange() {
        getLoaderManager().restartLoader(1, null, this);
    }
}
