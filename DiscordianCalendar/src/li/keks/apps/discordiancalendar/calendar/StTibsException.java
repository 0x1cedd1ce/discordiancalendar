package li.keks.apps.discordiancalendar.calendar;

/**
 * Created by ich on 21.09.13.
 */
public class StTibsException extends Exception {
    /**
     * Constructs a new {@code Exception} that includes the current stack trace.
     */
    public StTibsException() {
    }

    /**
     * Constructs a new {@code Exception} with the current stack trace and the
     * specified detail message.
     *
     * @param detailMessage the detail message for this exception.
     */
    public StTibsException(String detailMessage) {
        super(detailMessage);
    }

    /**
     * Constructs a new {@code Exception} with the current stack trace, the
     * specified detail message and the specified cause.
     *
     * @param detailMessage the detail message for this exception.
     * @param throwable     the cause of this exception.
     */
    public StTibsException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    /**
     * Constructs a new {@code Exception} with the current stack trace and the
     * specified cause.
     *
     * @param throwable the cause of this exception.
     */
    public StTibsException(Throwable throwable) {
        super(throwable);
    }
}
