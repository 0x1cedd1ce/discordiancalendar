package li.keks.apps.discordiancalendar.calendar;

import java.util.HashMap;

public class HolyDays {
    private static HashMap<Integer, String> HolyChaosDays = new HashMap<Integer, String>();
    private static HashMap<Integer, String> HolyDiscordDays = new HashMap<Integer, String>();
    private static HashMap<Integer, String> HolyConfusionDays = new HashMap<Integer, String>();
    private static HashMap<Integer, String> HolyBureaucracyDays = new HashMap<Integer, String>();
    private static HashMap<Integer, String> HolyTheAftermathDays = new HashMap<Integer, String>();

    private static HashMap<Integer, String> HolyChaosDaysDesc = new HashMap<Integer, String>();
    private static HashMap<Integer, String> HolyDiscordDaysDesc = new HashMap<Integer, String>();
    private static HashMap<Integer, String> HolyConfusionDaysDesc = new HashMap<Integer, String>();
    private static HashMap<Integer, String> HolyBureaucracyDaysDesc = new HashMap<Integer, String>();
    private static HashMap<Integer, String> HolyTheAftermathDaysDesc = new HashMap<Integer, String>();

    private static void putHolyDay(Season.ESeason season, int day, String name, String description) {
        description = description.replaceAll("\\.\\s*", ".\n");
        switch (season) {
            case CHAOS:
                HolyChaosDays.put(day, name);
                HolyChaosDaysDesc.put(day, description);
                break;
            case DISCORD:
                HolyDiscordDays.put(day, name);
                HolyDiscordDaysDesc.put(day, description);
                break;
            case CONFUSION:
                HolyConfusionDays.put(day, name);
                HolyConfusionDaysDesc.put(day, description);
                break;
            case BUREAUCRACY:
                HolyBureaucracyDays.put(day, name);
                HolyBureaucracyDaysDesc.put(day, description);
                break;
            case THE_AFTERMATH:
                HolyTheAftermathDays.put(day, name);
                HolyTheAftermathDaysDesc.put(day, description);
                break;
        }
    }

    static {
        putHolyDay(Season.ESeason.CHAOS, 1, "Nude Year's Day", "Observe the birth of the New Year, and record it so you can embarrass the year when it’s older. Give the New Year its first bath. Join it in the bath. Record that too. Try to determine whether it’s a boy or a girl, and then debate about it with yourself.  Then debate about what difference it makes.  Optional: Do all the above with family, friends, and complete strangers.  In honor of the newborn year, celebrants should wear only what they wore when they were brand spanking new on their true birthdays.   This is also known as Birthday Suit Day, which can be celebrated on your or anybody else's birthday.");
        putHolyDay(Season.ESeason.CHAOS, 5, "Mungday", "Contemplate The Sacred Chao that Apostle Hung Mung devised (for Ek-sen-trik Discordians, ideally the one on Saint The Mary’s belly.  Contemplating her navel and other body parts as well counts extra).  Think about the symbolism involved, the meaning, the Hodge Podgeness of it all.  Then go do something fun.");
        putHolyDay(Season.ESeason.CHAOS, 10, "Binary Day / Backwards Day, Reformed", " (more properly, demrofeR, yaD sdrawkcaB) Day to do things backwards.  Walk backwards, dress backwards, talk backwards.  (Driving backwards on a busy freeway is not recommended.  In some jurisdictions, it’ s not advisable to wear a back-less dress or thong bikini backwards.  But it could be fun).  Doing or wearing things inside out or upside down also count.  Check your favorite recordings for backwards masking.  Pray to sirE.  Hate what you love, love what you hate. This is also a good day to switch positions with a superior or inferior as described for April Fool’s Day.  Children can tell their parents, “Go clean your room and do your homework,” and parents can tell their children, “If you don’t eat your dessert, you won’t get any vegetables.”  Unless, of course, that’s your usual pattern.  (Note that we chose this date as Backwards Day because both the Gregorian and Discordian Dates can be written as 01 10 or 10 01, both of which are the same backwards.) For those who recognize Chaos 26 as Backwards Day.  It’s basically the same holiday, but focuses more on the bi part.");
        putHolyDay(Season.ESeason.CHAOS, 18, "Pat Pineapple Day", " Honor the ECG Mascot (and Ek-sen-trik Discordian Brigadier and Patron Saint) by consuming something with or associated with pineapple.  Creativity is encouraged.  This is the only day the Order of the Pineapple can be presented.");
        putHolyDay(Season.ESeason.CHAOS, 21, "Hug Day", "Hug your friends.  Hug your loved ones.  Confound your enemies by offering them a hug.  Hugs are free, hugs are healthy, hugs are good.  Which is why they may be illegal in some jurisdictions.");
        putHolyDay(Season.ESeason.CHAOS, 26, "Backwards Day, Traditional", " (See Chaos 10)");
        putHolyDay(Season.ESeason.CHAOS, 49, "The Mary Day", " Honor The Mary, who is Patron Saint, Illuminated, of Bearers of Erisian Tattoos, and also Keeper of the Holy Chao Belly (rub it for good luck--with permission, of course).  This is a good day to do something for a Mary, do something Merry, or even to Marry someone.  (If you are merry about marrying a Mary, you earn extra points).");
        putHolyDay(Season.ESeason.CHAOS, 50, "Chaoflux", "Holyday of the Season of Chaos.  We have no idea how to celebrate this day.  The Principia Discordia doesn’t tell us.  Do something chaotic, we suppose.");
        putHolyDay(Season.ESeason.CHAOS, 51, "Pet Loving Day", "A day to love your pet, or someone else’s (willing pets only). Can a holy day be illegal?");
        putHolyDay(Season.ESeason.CHAOS, 69, "Head Chicken / Chicken Head Day", "Can only be celebrated with a partner (a flock of partners could work).  Tarring and feathering each other before you begin is optional.  You and your partner must be plucked (i.e. au naturel), declare each other chickens, then simultaneously peck each other.  Continue simultaneous pecking to determine who has the best Chicken Head and/or who’s the best Head Chicken.  See who comes in first, and who comes in second.  Then declare yourselves both winners.  (If you want to know where you peck each other, check the Discordian date for a clue.  Another hint: this is also known as Head-Tail/Tail- Head Day.  The holiday can also be celebrated on the 6th day of the 9th month, the 6th month on the 9th day, or any day, really).");

        putHolyDay(Season.ESeason.DISCORD, 5, "Mojoday", " (for those of Lord Omar’s sect, Lingananday)  Have an argument with a friend over which Five Star Saint’s holyday it really is--especially in front of people who haven’t the faintest idea what you’re talking about.  Take turns chanting “Mojo” and Lingananda.”  Switch sides.  Use as many obscure Discordian references as possible.  Good places for this are parks, buses, and city council meetings.");
        putHolyDay(Season.ESeason.DISCORD, 11, "Discordians for Jesus / Love your Neighbor Day", "Do you love Jesus?  Would you like to love your neighbor?  Then this is the holiday for you!  Tell your Fundamentalist Christian neighbors that the tradition-breaking, authority-challenging, logic- twisting, wise-cracking, trouble-making Jesus of Nazareth was really a proto- Discordian.  Then ask them if they want to love you back.  Be prepared to run.");
        putHolyDay(Season.ESeason.DISCORD, 18, "April Fool's Day", "Fool friends for fun.  Fool enemies for fun.  Fool yourself for even more fun.  If possible, switch positions/possessions/clothing with your boss, employee, teacher, student, parent, child, master, slave, etc. for the day.  Let them see how hard it really is to be you.  See how easy it is to be them.");
        putHolyDay(Season.ESeason.DISCORD, 19, "St. John the Blasphemist's Day", "Commit blasphemy against Goddess Discordia!  Write your representatives and demand they outlaw \"funny religions;\" send one hour's salary to the most Aneristic organization you can think of; or eat a bun with a hot dog made out of the Sacred Chao.  On second thought, recognizing a holy day wouldn't be blasphemist.  So instead of actually celebrating it, call in sick to work or school and claim a migraine or diarrhea or a swollen pineal gland.  Then spend all day sleeping, going fishing or having sex.");
        putHolyDay(Season.ESeason.DISCORD, 23, "Jake Day", "Get a group of people to send letters, postcards, faxes, emails, etc. all to hopefully arrive at some business or government agency you want to Jake on this day (you’re allowed to do Jakes on other days as well, of course).  The messages should address non-existent concerns, imaginary problems, or be just plain weird.  Examples: assert that your family’s woolly mammoth should be allowed to run for king, or that you’re positive the president is actually a pineapple.  Jakes should be fun, not threatening, and are all part of Operation Mindfuck.");
        putHolyDay(Season.ESeason.DISCORD, 50, "Discoflux", "Holyday of the Season of Discord.  Again, the Principia Discordia doesn’t tell us what to do, but do something that promotes Discord, or maybe Disco.   Dress as you would to go to a disco sometime in the 1950s through 1970s, imagine you’re the Disco King or Queen (or both), then begin dancing to the music in your head.  Pretend the discotheque is at your local market, a playground, or in front of a house of worship.  Leave before the police arrive.  If one shows up before you depart, tell the officer, “Love the threads.   Wanna boogie?”  If the officer declines the invitation, apologize profusely for your mistake and claim you forgot to take your medication.  Better yet, forget the whole thing.");
        putHolyDay(Season.ESeason.DISCORD, 70, "Jake Day Jr. / Day of the Elppin", "Pull some small Jakes or practical jokes, especially those that cause embarrassment or the loss of personal modesty.  In other words, do like the Elppin do.  Remember, junior jakes should be fun!  And legal.  Don't forget legal.");
        putHolyDay(Season.ESeason.DISCORD, 71, "Towel Day", "Are you one hoopy frood who really knows where your towel is?  Well, you should be!  Keep a towel with you all day today.  People respect those who carry a towel.  And if you meet someone who is too foolish to show you respect, use your towel to cover your eyes.  They'll assume that if you can't see them, they can't see you, and they'll leave you alone.  At least that's the theory.");

        putHolyDay(Season.ESeason.CONFUSION, 5, "Syaday", "(some might call it Gulikday)/Fearless Fred Day (ED).  We won’t get in an argument over whether this holiday truly belongs to St. Sri Syadasti (etc.) or to St. Gulik the Stoned.  Because of course it belongs to St. Fearless Fred.  Honor the Five Star Saint and President of planet Ek-sen-trik-kuh by walking barefoot, riding a moped, or climbing up a rose trellis to rescue a maiden in distress and/or wrestle a maiden out of her dress (willing maidens only).  Call everyone and everything “Fred.”  This is also known as Fredday.");
        putHolyDay(Season.ESeason.CONFUSION, 11, "537 Day", "(See Aftermatch 37)");
        putHolyDay(Season.ESeason.CONFUSION, 15, "Mad Hatter Day", "(See Bureaucracy 60)");
        putHolyDay(Season.ESeason.CONFUSION, 26, "Imaginary Friend / Captain Tuttle Day", "Recognize and credit your imaginary friend, whom you blame for everything on all the other days of the year.  Or help someone anonymously, but in a way they’d never expect.  (“Captain Tuttle” was the made- up buddy of Benjamin Franklin “Hawkeye” Pierce and “Trapper John” McIntire on the TV show “M*A*S*H.”  They used the mythical captain’s identity to get supplies and funds from the U. S. Military which were then sent to help an orphanage).");
        putHolyDay(Season.ESeason.CONFUSION, 37, "Mid Year's Day", "The middle day of the year.  Use only middle names, stay to the middle of the road, pat or rub your or someone else’s middle (this is a great day to apply to join The Order of the Holy Chao Belly Rubbers).  Point to things with your middle finger.  Visit a middle school and talk about the good old days of the Middle Ages.  (If you want to get technical, on Leap Year this day would run from noon of July 2 to noon of July 3, but you don’ t have to get that anal unless you really want to).");
        putHolyDay(Season.ESeason.CONFUSION, 50, "Confuflux", "Holyday of the Season of Confusion.  How do you celebrate this day?  We don’t know--we’re confused.");

        putHolyDay(Season.ESeason.BUREAUCRACY, 3, "Multiversal Underwear Day", "Expose your undies!   Celebrate by wearing nothing but unmentionables in public, or at least by wearing your underwear, or somebody else’s, where people can see it so they can gawk at you.  (This is a great day to apply for membership in the Lesser Disorder of Underwear Heads.)");
        putHolyDay(Season.ESeason.BUREAUCRACY, 5, "Zaraday", "Celebrate the Apostle Zarathud by studying the Five Commandments (the Pentabarf) the Apostle gave us.  Carve a copy of them into a stone tablet (do this in advance--it takes a while).  Insist the tablet be planted in the lawn of your local courthouse.  If they refuse, claim religious discrimination.");
        putHolyDay(Season.ESeason.BUREAUCRACY, 18, "Festival of Hanky-Panky Spankies", "The Mythics version of Spanking Fest--(See Aftermath 36).");
        putHolyDay(Season.ESeason.BUREAUCRACY, 33, "Cat Dancing & Foot Fetish Day aka Pussyfoot Day", "(also Foot Fetish Day, or Pussyfoot Day) (ED).  Dance with a Cat, Dance like a Cat, Dance with somebody who has a Cat, or just do something having to do with Cats and/or Dancing (You’re allowed to be creative in what you consider to be a “cat” and “dancing”).  Or do something having to do with Feet--after all, cats have feet, and feet dance.  Foot note: Whipped cream and chocolate syrup make great foot toppings.  Only advanced foot fetishists should use Catsup.");
        putHolyDay(Season.ESeason.BUREAUCRACY, 37, "Mass of Planet Eris / Eristotle", "Even the scientific bureaucracy recognized our Goddess (& the Mythic Prophet Eristotle) by naming Planet X Eris on this date in 2006 CE.  As of this writing, they're calling this world a dwarf planet, but we don't mind.  We have nothing against dwarves.  So celebrate our planet's mass!  (And remember, Eris' moon Dysmonia is really Shamlicht.)");
        putHolyDay(Season.ESeason.BUREAUCRACY, 50, "Bureflux", "Holyday of the Season of Bureaucracy.  Why doesn’t the Principia Discordia tell us what to do with these seasonal holidays anyway?  They should form a bureaucracy to figure that out.  Write to your representatives and tell them that.   Write in Ancient Greek.  Use a purple crayon.");
        putHolyDay(Season.ESeason.BUREAUCRACY, 57, "Shamlicht Kids Club Day", "Honor the leaders and members of Smagmoid Kids Club.  Lead the “Shamlicht Kids Club Song” at a club, organization or school board meeting; call a school and order five boxes of Twin Mints or Chock-a-lot Chaochip cookies; ask a friend or a total stranger to join you in the Bodyshake.  Realize that no adult ever fully grows out of childhood, and that children can be very adult.");
        putHolyDay(Season.ESeason.BUREAUCRACY, 59, "Gonkulator Day", "(original spelling, Gonculator Day). A time to recognize impressive and sophisticated-seeming devices that are completely useless.  See how many you can find in your home or office.  See how many you can sell for a large profit to those who don’t know what day this is.  Those who work for major computer corporations should find this an easy holiday to celebrate.  (This holiday was inspired by an episode of Hogan’s Heroes that first aired on this date.  The prisoners of war convinced members of the enemy’s Military Bureaucracy that a prisoner-made rabbit trap was actually some highly important device known as a “gonculator.”  The scheme worked because no one in the military establishment was willing to admit they didn’t have the foggiest idea what the prisoners were talking about.)");
        putHolyDay(Season.ESeason.BUREAUCRACY, 60, "Mad Hatter Day", "Go mad-- without inhaling mercury or getting committed. Host a mad tea party wearing a 10/6 size hat, or just join in.  (See Confusion 15)");
        putHolyDay(Season.ESeason.BUREAUCRACY, 66, "Habeas Corpus Remembrance Day", "Celebrated in honor of the death of Eristotle on this date in 1782, and Emperor Norton I declaring the United States dissolved on this date in 1859 CE.  A day of Fondling and Groping, where all Discordians should grab their Legislative Representative by the privates and cry out, \"Where's my Bill of Rights, you leech!\"  (Note: grabbing should be figurative, not literal).");

        putHolyDay(Season.ESeason.THE_AFTERMATH, 5, "Maladay", "Possible ways to recognize The Elder Malaclypse include carrying around a sign that says “DUMB;” following anyone who’s wearing a pentagram and, when they stop, demanding they tell you where the manger is; or asking merchants at a mall where the late J. R. R. Tolkien is doing book signings.  You could also ask how to get to the local Beatles concert.  This has nothing to do with Malaclypse, but could be really cool if someone gives you actual directions.");
        putHolyDay(Season.ESeason.THE_AFTERMATH, 28, "Ek-sen-triks CluborGuild Day", "This is the anniversary of the founding of Chapter One of the ECG, which was officially recognized and approved by a California college Student Government Association on Monday, 16 November, 1981 (Setting Orange, 28 Aftermath 3147).  Within a year after its official recognition, it was honored as Most Active New Club.  Within that same year, both the Founding President and Co-Founding Vice President created so much chaos and discord at the college they were threatened with formal charges.  Honor the ECG.");
        putHolyDay(Season.ESeason.THE_AFTERMATH, 36, "Spanking Fest", "Do you like spanking?  Do you like getting spanked?  Do you have friends and relatives who do too?  Then today is your day to go for it.   Another day to go for it is 18 Bureaucracy.");
        putHolyDay(Season.ESeason.THE_AFTERMATH, 37, "537 Day / Turkey Day", " This is the 5th season, 37th day.  Contemplate the number 537, Holy Number of Discordian Ek-sen-triks and Official Number of the ECG (who celebrate it using the Gregorian calendar on the 5th month, 37th day).  Consult your pineal gland to try to discover the number’s true meaning.  Or type the number “537” on your computer, then stare at it while hanging upside down for five hours and thirty-seven minutes (Optional: remain upright, and hold your computer monitor upside down for the same length of time).  About once every seven years in America, this is also known as Turkey Day (O).");
        putHolyDay(Season.ESeason.THE_AFTERMATH, 46, "Hug Day II", "See Hug Day on 21 Chaos.");
        putHolyDay(Season.ESeason.THE_AFTERMATH, 50, "Afflux", "Holyday of the Season of Aftermath.  Thank Goddess this is the last seasonal holyday we have to figure out something to do for.  Thank Goddess it’s getting toward the end of the year.  Thank Goddess the aftermath of what you did all year isn’t too bad.  Well, not as bad as it could have been.");
        putHolyDay(Season.ESeason.THE_AFTERMATH, 67, "Santa Claus Day", "Celebrate the Patron Saint of Children.  Honor the jolly old man who loves children, wears funny clothes, has no legal address, holds boys and girls on his lap, urges them to share their desires with him, offers them a candy cane, dreams of sliding down their chimneys, sneaks into their homes at night, watches them when they’re in bed asleep, then gives them a package to unwrap.  (This holiday is also known as Christmas.  It’s also known as Don’t You Think We’ve Gotten So Paranoid About This sWhole Protect Our Children Thing That We’re Actually Harming Them and Ourselves Day).");
        putHolyDay(Season.ESeason.THE_AFTERMATH, 72, "New Year's Eve Eve", "Began with an actual ECG New Year’s Eve Eve party in 1983 or 1984 that was written up in a pretty big newspaper (yes, this is true).  See if you can still be celebrating when New Year’s Eve comes around.  See if you can still stand up.");
    }

    public static String getHolyDay(DiscordianDay day) {
        switch (day.getSeason()) {
            case CHAOS:
                if (HolyChaosDays.containsKey(day.getDay())) {
                    return HolyChaosDays.get(day.getDay());
                }
                return "";
            case DISCORD:
                if (HolyDiscordDays.containsKey(day.getDay())) {
                    return HolyDiscordDays.get(day.getDay());
                }
                return "";
            case CONFUSION:
                if (HolyConfusionDays.containsKey(day.getDay())) {
                    return HolyConfusionDays.get(day.getDay());
                }
                return "";
            case BUREAUCRACY:
                if (HolyBureaucracyDays.containsKey(day.getDay())) {
                    return HolyBureaucracyDays.get(day.getDay());
                }
                return "";
            case THE_AFTERMATH:
                if (HolyTheAftermathDays.containsKey(day.getDay())) {
                    return HolyTheAftermathDays.get(day.getDay());
                }
                return "";
            case ST_TIBS_DAY:
                return "St. Tibs Day";
            default:
                return "";
        }
    }

    public static String getHolyDayDescription(DiscordianDay day) {
        switch (day.getSeason()) {
            case CHAOS:
                if (HolyChaosDaysDesc.containsKey(day.getDay())) {
                    return HolyChaosDaysDesc.get(day.getDay());
                }
                return "";
            case DISCORD:
                if (HolyDiscordDaysDesc.containsKey(day.getDay())) {
                    return HolyDiscordDaysDesc.get(day.getDay());
                }
                return "";
            case CONFUSION:
                if (HolyConfusionDaysDesc.containsKey(day.getDay())) {
                    return HolyConfusionDaysDesc.get(day.getDay());
                }
                return "";
            case BUREAUCRACY:
                if (HolyBureaucracyDaysDesc.containsKey(day.getDay())) {
                    return HolyBureaucracyDaysDesc.get(day.getDay());
                }
                return "";
            case THE_AFTERMATH:
                if (HolyTheAftermathDaysDesc.containsKey(day.getDay())) {
                    return HolyTheAftermathDaysDesc.get(day.getDay());
                }
                return "";
            case ST_TIBS_DAY:
                return "Tell everyone today is your birthday.  Figure out how old you would be if you only had a birthday once every leap year.   Act your age.  Determine if you’re of legal age to drive, raise children, buy a gun, gamble, leave school, get a job, drink, get married, get drafted, sign a contract, vote, run for parliament or congress, have sexual congress, have sexual congress for money, retire, or walk across the street by yourself.  Realize how silly having a legal age for everything really is, and how arbitrary it is, and how it doesn’t recognize the individual as an individual, and how it’s all culturally-biased ageism anyway.  Forget about age limits, and do whatever you want.  If you’re lucky, maybe you can convince them you’re too young to get arrested for doing any of the above (but don’t count on it).";
            default:
                return "";
        }
    }
}

