package li.keks.apps.discordiancalendar.calendar;

public final class Season {
    public enum ESeason implements Comparable<ESeason> {
        CHAOS,
        DISCORD,
        CONFUSION,
        BUREAUCRACY,
        THE_AFTERMATH,
        ST_TIBS_DAY;

        public ESeason increment() {
            switch (this) {
                case CHAOS:
                    return DISCORD;
                case DISCORD:
                    return CONFUSION;
                case CONFUSION:
                    return BUREAUCRACY;
                case BUREAUCRACY:
                    return THE_AFTERMATH;
                case THE_AFTERMATH:
                    return CHAOS;
                case ST_TIBS_DAY:
                    return ST_TIBS_DAY;
                default:
                    return ST_TIBS_DAY;
            }
        }

        public ESeason decrement() {
            switch (this) {
                case CHAOS:
                    return THE_AFTERMATH;
                case DISCORD:
                    return CHAOS;
                case CONFUSION:
                    return DISCORD;
                case BUREAUCRACY:
                    return CONFUSION;
                case THE_AFTERMATH:
                    return BUREAUCRACY;
                case ST_TIBS_DAY:
                    return ST_TIBS_DAY;
                default:
                    return ST_TIBS_DAY;
            }
        }
    }
}
