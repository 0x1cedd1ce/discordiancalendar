package li.keks.apps.discordiancalendar.calendar;

import android.text.format.DateUtils;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import li.keks.apps.discordiancalendar.calendar.Season.ESeason;
import li.keks.apps.discordiancalendar.calendar.Weekday.EWeekday;

public class DiscordianDay implements Serializable, Comparable<DiscordianDay> {
    public static final int TAG_ID = 0;
    /**
     *
     */
    private static final long serialVersionUID = -988291216942583991L;
    private ESeason m_eSeason;
    private EWeekday m_eWeekday;
    private GregorianCalendar m_gregorianDay;
    private int m_nDay;
    private int m_nYear;
    private String m_sHolyDay;
    private String m_sHolyDayDesc;

    public DiscordianDay(int nDay, ESeason eSeason, int nYear) {
        m_nDay = nDay;
        m_eWeekday = Weekday.getWeekday(nDay + eSeason.ordinal() * 73);
        m_eSeason = eSeason;
        m_nYear = nYear;
        calculateGregorianDate();
    }

    public DiscordianDay(Date gregorianDay) {
        m_gregorianDay = new GregorianCalendar();
        m_gregorianDay.setTime(gregorianDay);
        calculateDiscordianDate();
    }

    private void calculateDiscordianDate() {
        m_nYear = m_gregorianDay.get(Calendar.YEAR) + 1166;
        m_eWeekday = Weekday.getWeekday(m_gregorianDay.get(Calendar.DAY_OF_YEAR));
        m_nDay = m_gregorianDay.get(Calendar.DAY_OF_MONTH);
        switch (m_gregorianDay.get(Calendar.MONTH)) {
            case Calendar.JANUARY:
                m_eSeason = ESeason.CHAOS;
                break;
            case Calendar.FEBRUARY:
                if (m_nDay == 29) {
                    m_eSeason = ESeason.ST_TIBS_DAY;
                    m_nDay = 0;
                    m_eWeekday = EWeekday.ST_TIBS_DAY;
                } else {
                    m_eSeason = ESeason.CHAOS;
                    m_nDay += 31;
                }
                break;
            case Calendar.MARCH:
                m_nDay += 59;
                if (m_nDay <= 73) {
                    m_eSeason = ESeason.CHAOS;
                } else {
                    m_nDay -= 73;
                    m_eSeason = ESeason.DISCORD;
                }
                break;
            case Calendar.APRIL:
                m_nDay += 17;
                m_eSeason = ESeason.DISCORD;
                break;
            case Calendar.MAY:
                m_nDay += 47;
                if (m_nDay <= 73) {
                    m_eSeason = ESeason.DISCORD;
                } else {
                    m_nDay -= 73;
                    m_eSeason = ESeason.CONFUSION;
                }
                break;
            case Calendar.JUNE:
                m_nDay += 5;
                m_eSeason = ESeason.CONFUSION;
                break;
            case Calendar.JULY:
                m_nDay += 35;
                m_eSeason = ESeason.CONFUSION;
                break;
            case Calendar.AUGUST:
                m_nDay += 66;
                if (m_nDay <= 73) {
                    m_eSeason = ESeason.CONFUSION;
                } else {
                    m_nDay -= 73;
                    m_eSeason = ESeason.BUREAUCRACY;
                }
                break;
            case Calendar.SEPTEMBER:
                m_nDay += 24;
                m_eSeason = ESeason.BUREAUCRACY;
                break;
            case Calendar.OCTOBER:
                m_nDay += 54;
                if (m_nDay <= 73) {
                    m_eSeason = ESeason.BUREAUCRACY;
                } else {
                    m_nDay -= 73;
                    m_eSeason = ESeason.THE_AFTERMATH;
                }
                break;
            case Calendar.NOVEMBER:
                m_nDay += 12;
                m_eSeason = ESeason.THE_AFTERMATH;
                break;
            case Calendar.DECEMBER:
                m_nDay += 42;
                m_eSeason = ESeason.THE_AFTERMATH;
                break;
        }
        updateHolyDay();
    }

    private void calculateGregorianDate() {
        m_gregorianDay = new GregorianCalendar();
        m_gregorianDay.set(Calendar.AM_PM, Calendar.AM);
        m_gregorianDay.set(Calendar.YEAR, m_nYear - 1166);
        int nLeap = 0;
        if (m_gregorianDay.isLeapYear(m_nYear - 1166)) {
            nLeap = 1;
        }
        switch (m_eSeason) {
            case CHAOS:
                if (m_nDay < 29) {
                    m_gregorianDay.set(Calendar.DAY_OF_YEAR, m_nDay);
                } else {
                    m_gregorianDay.set(Calendar.DAY_OF_YEAR, m_nDay + nLeap);
                }
                break;
            case DISCORD:
                m_gregorianDay.set(Calendar.DAY_OF_YEAR, m_nDay + 73 + nLeap);
                break;
            case CONFUSION:
                m_gregorianDay.set(Calendar.DAY_OF_YEAR, m_nDay + 73 * 2 + nLeap);
                break;
            case BUREAUCRACY:
                m_gregorianDay.set(Calendar.DAY_OF_YEAR, m_nDay + 73 * 3 + nLeap);
                break;
            case THE_AFTERMATH:
                m_gregorianDay.set(Calendar.DAY_OF_YEAR, m_nDay + 73 * 4 + nLeap);
                break;
            case ST_TIBS_DAY:
                m_gregorianDay.set(Calendar.MONTH, Calendar.FEBRUARY);
                m_gregorianDay.set(Calendar.DAY_OF_MONTH, 29);
                break;
        }
        updateHolyDay();
    }

    /**
     * Compares this object to the specified object to determine their relative
     * order.
     *
     * @param another the object to compare to this instance.
     * @return a negative integer if this instance is less than {@code another};
     * a positive integer if this instance is greater than
     * {@code another}; 0 if this instance has the same order as
     * {@code another}.
     * @throws ClassCastException if {@code another} cannot be converted into something
     *                            comparable to {@code this} instance.
     */
    @Override
    public int compareTo(DiscordianDay another) {
        if (m_nYear < another.getYear()) {
            return -1;
        } else if (m_nYear > another.getYear()) {
            return 1;
        } else {
            if (m_eSeason.compareTo(another.getSeason()) < 0) {
                return -1;
            } else if (m_eSeason.compareTo(another.getSeason()) > 0) {
                return 1;
            } else {
                if (m_nDay < another.getDay()) {
                    return -1;
                } else if (m_nDay > another.getDay()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }
    }

    public int getDay() {
        return m_nDay;
    }

    public void setDay(int nDay) {
        this.m_nDay = nDay;
        calculateGregorianDate();
    }

    public Date getGregorianDate() {
        return m_gregorianDay.getTime();
    }

    public GregorianCalendar getGregorianDay() {
        return (GregorianCalendar) m_gregorianDay.clone();
    }

    public void setGregorianDay(Calendar gregorianDay) {
        m_gregorianDay.setTime(gregorianDay.getTime());
        calculateDiscordianDate();
    }

    public void setGregorianDay(Date gregorianDay) {
        m_gregorianDay.setTime(gregorianDay);
        calculateDiscordianDate();
    }

    public int getHour() {
        return m_gregorianDay.get(Calendar.HOUR_OF_DAY);
    }

    public int getMinute() {
        return m_gregorianDay.get(Calendar.MINUTE);
    }

    public String getHolyDay() {
        return m_sHolyDay;
    }

    public String getHolyDayDesc() {
        return m_sHolyDayDesc;
    }

    private void updateHolyDay() {
        m_sHolyDay = HolyDays.getHolyDay(this);
        m_sHolyDayDesc = HolyDays.getHolyDayDescription(this);
    }

    public ESeason getSeason() {
        return m_eSeason;
    }

    public void setSeason(ESeason eSeason) {
        this.m_eSeason = eSeason;
        calculateGregorianDate();
    }

    public int getSecond() {
        return m_gregorianDay.get(Calendar.SECOND);
    }

    public EWeekday getWeekday() {
        return m_eWeekday;
    }

    public int getYear() {
        return m_nYear;
    }

    public void setYear(int nYear) {
        this.m_nYear = nYear;
        calculateGregorianDate();
    }

    public String toString() {
        return m_nDay + " " + m_eWeekday.toString() + " " + m_eSeason.toString() + " "
                + m_nYear;
    }

    public boolean isToday() {
        return DateUtils.isToday(m_gregorianDay.getTimeInMillis());
    }

    public DiscordianDay inc() {
        GregorianCalendar cal = this.getGregorianDay();
        cal.add(Calendar.DAY_OF_YEAR, 1);
        DiscordianDay newDay = new DiscordianDay(cal.getTime());
        return newDay;
    }

    public DiscordianDay dec() {
        GregorianCalendar cal = this.getGregorianDay();
        cal.add(Calendar.DAY_OF_YEAR, -1);
        DiscordianDay newDay = new DiscordianDay(cal.getTime());
        return newDay;
    }
}
