package li.keks.apps.discordiancalendar.calendar;

public class Weekday {
    public static EWeekday getWeekday(int nDay) {
        switch (nDay % 5) {
            case 0:
                return EWeekday.SWEETMORN;
            case 1:
                return EWeekday.BOOMTIME;
            case 2:
                return EWeekday.PUNGENDAY;
            case 3:
                return EWeekday.PRICKLE_PRICKLE;
            case 4:
                return EWeekday.SETTING_ORANGE;
        }
        return EWeekday.ST_TIBS_DAY;
    }

    public static enum EWeekday {
        SWEETMORN("Sweetmorn"),
        BOOMTIME("Boomtime"),
        PUNGENDAY("Pungenday"),
        PRICKLE_PRICKLE("Prickle-Prickle"),
        SETTING_ORANGE("Setting Orange"),
        ST_TIBS_DAY("St. Tibs Day");
        private final String m_sName;

        EWeekday(String sName) {
            this.m_sName = sName;
        }

        public String toString() {
            return m_sName;
        }
    }
}
