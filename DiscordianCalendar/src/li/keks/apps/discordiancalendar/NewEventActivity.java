package li.keks.apps.discordiancalendar;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.MenuItem;

import java.util.TimeZone;

import li.keks.apps.discordiancalendar.calendar.DiscordianDay;

//import android.support.v4.app.NavUtils;

/**
 * An activity representing a single Season detail screen. This activity is only
 * used on handset devices. On tablet-size devices, item details are presented
 * side-by-side with a list of items in a {@link SeasonListActivity}.
 * <p/>
 * This activity is mostly just a 'shell' activity containing nothing more than
 * a {@link DayListFragment}.
 */
public class NewEventActivity extends Activity implements TimezoneDialog.TimezoneDialogListener,
        TimeDialog.TimeDialogListener, DateDialog.DateDialogListener {

    private NewEventFragment mFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);

        // Show the Up button in the action bar.
        getActionBar().setDisplayHomeAsUpEnabled(true);

        mFragment = new NewEventFragment();
        mFragment.setArguments(getIntent().getExtras());
        getFragmentManager().beginTransaction()
                .add(R.id.new_event_container, mFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                //NavUtils.navigateUpTo(this, new Intent(this,
                //        SeasonListActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateDialogClick(DateDialog dialog, DiscordianDay day, boolean begin) {
        mFragment.onDateDialogClick(dialog, day, begin);
    }

    @Override
    public void onTimeDialogClick(TimeDialog dialog, int hour, int minute, boolean begin) {
        mFragment.onTimeDialogClick(dialog, hour, minute, begin);
    }

    @Override
    public void onTimezoneDialogClick(DialogFragment dialog, TimeZone tz) {
        mFragment.onTimezoneDialogClick(dialog, tz);
    }
}
