package li.keks.apps.discordiancalendar;

import android.app.Activity;
import android.app.Fragment;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import li.keks.apps.discordiancalendar.calendar.DiscordianDay;
import li.keks.apps.discordiancalendar.calendar.Season;

/**
 * A list fragment representing a list of Seasons. This fragment also supports
 * tablet devices by allowing list items to be given an 'activated' state upon
 * selection. This helps indicate which item is currently being viewed in a
 * {@link DayListFragment}.
 * <p/>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class SeasonListFragment extends Fragment implements GridView.OnItemClickListener,
        GestureOverlayView.OnGesturePerformedListener,
        View.OnClickListener, CalendarObserverCallback {

    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";
    /**
     * The serialization (saved instance state) Bundle key representing the
     * season
     */
    private static final String STATE_SEASON = "season";
    /**
     * The serialization (saved instance state) Bundle key representing the
     * year
     */
    private static final String STATE_YEAR = "year";

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static final Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(DiscordianDay mDay) {
        }
    };
    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = GridView.INVALID_POSITION;
    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */
    private Callbacks mCallbacks = sDummyCallbacks;
    /**
     * The GridView
     */
    private GridView m_GridView;
    /**
     * the left TextView
     */
    private TextView m_leftTextView;
    /**
     * the right TextView
     */
    private TextView m_rightTextView;
    /**
     * The shown season
     */
    private Season.ESeason m_Season;
    /**
     * The shown year
     */
    private int m_Year;
    /**
     * Library with gestures
     */
    private GestureLibrary gestureLib;
    /**
     * Content Observer
     */
    private CalendarObserver mContentObserver;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SeasonListFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.textViewRight) {
            if (this.m_Season == Season.ESeason.THE_AFTERMATH) {
                this.m_Year++;
            }
            this.setSeason(this.m_Season.increment(), this.m_Year);
        } else if (v.getId() == R.id.textViewLeft) {
            if (this.m_Season == Season.ESeason.CHAOS) {
                this.m_Year--;
            }
            this.setSeason(this.m_Season.decrement(), this.m_Year);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Date gToday = new Date();
        DiscordianDay dToday = new DiscordianDay(gToday);
        this.m_Season = dToday.getSeason();
        this.m_Year = dToday.getYear();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LinearLayout rootView = (LinearLayout) inflater.inflate(R.layout.fragment_season_list,
                container, false);

        this.m_GridView = (GridView) rootView.findViewById(R.id.gridView);
        this.m_GridView.setOnItemClickListener(this);

        this.m_leftTextView = (TextView) rootView.findViewById(R.id.textViewLeft);
        this.m_leftTextView.setOnClickListener(this);

        this.m_rightTextView = (TextView) rootView.findViewById(R.id.textViewRight);
        this.m_rightTextView.setOnClickListener(this);

        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_SEASON)) {
            this.m_Season = Season.ESeason.valueOf(savedInstanceState.getString(STATE_SEASON));
        }
        if (savedInstanceState != null && savedInstanceState.containsKey(STATE_YEAR)) {
            this.m_Year = savedInstanceState.getInt(STATE_YEAR);
        }
        setSeason(this.m_Season, this.m_Year);

        GestureOverlayView gestureOverlayView = new GestureOverlayView(this.getActivity());
        gestureOverlayView.addView(rootView);
        gestureOverlayView.addOnGesturePerformedListener(this);
        gestureLib = GestureLibraries.fromRawResource(this.getActivity(), R.raw.gestures);
        if (!gestureLib.load()) {
            this.getActivity().finish();
        }
        return gestureOverlayView;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }

    /**
     * Callback method to be invoked when an item in this AdapterView has
     * been clicked.
     * <p/>
     * Implementers can call getItemAtPosition(position) if they need
     * to access the data associated with the selected item.
     *
     * @param parent   The AdapterView where the click happened.
     * @param view     The view within the AdapterView that was clicked (this
     *                 will be a view provided by the adapter)
     * @param position The position of the view in the adapter.
     * @param id       The row id of the item that was clicked.
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mCallbacks.onItemSelected((DiscordianDay) this.m_GridView.getItemAtPosition(position));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != GridView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
        outState.putString(STATE_SEASON, this.m_Season.toString());
        outState.putInt(STATE_YEAR, this.m_Year);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState
                    .getInt(STATE_ACTIVATED_POSITION));
        }

    }


    @Override
    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
        if (gesture.getLength() > 300) {
            ArrayList<Prediction> predictions = gestureLib.recognize(gesture);
            for (Prediction prediction : predictions) {
                if (prediction.score > 1.0) {
                    if (prediction.name.equals("next")) {
                        if (this.m_Season == Season.ESeason.THE_AFTERMATH) {
                            this.m_Year++;
                        }
                        this.setSeason(this.m_Season.increment(), this.m_Year);
                    } else if (prediction.name.equals("prev")) {
                        if (this.m_Season == Season.ESeason.CHAOS) {
                            this.m_Year--;
                        }
                        this.setSeason(this.m_Season.decrement(), this.m_Year);
                    }
                }
            }
        }
    }

    /**
     * Sets the new Season
     */
    private void setSeason(Season.ESeason eSeason, int nYear) {
        this.m_Season = eSeason;
        this.m_Year = nYear;
        CalendarListAdapter listAdapter =
                new CalendarListAdapter(this.getActivity(), this.m_Season, this.m_Year);
        this.m_GridView.setAdapter(listAdapter);

        if (mContentObserver == null) {
            mContentObserver = new CalendarObserver(new Handler(), this);
            getActivity().getContentResolver().registerContentObserver(CalendarContract.CONTENT_URI, true, mContentObserver);
        }
        getLoaderManager().restartLoader(0, null, listAdapter);

        m_leftTextView.setText(this.m_Season.decrement().name() + " — " + this.m_Year);
        m_rightTextView.setText(this.m_Season.increment().name() + " — " + this.m_Year);

        getActivity().setTitle(this.m_Season.name() + " — " + this.m_Year);
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mContentObserver != null) {
            getActivity().getContentResolver().unregisterContentObserver(mContentObserver);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mContentObserver != null) {
            getActivity().getContentResolver().registerContentObserver(CalendarContract.CONTENT_URI, true, mContentObserver);
        }
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        m_GridView.setChoiceMode(
                activateOnItemClick ? GridView.CHOICE_MODE_SINGLE
                        : GridView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == GridView.INVALID_POSITION) {
            m_GridView.setItemChecked(mActivatedPosition, false);
        } else {
            m_GridView.setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }

    @Override
    public void onChange() {
        getLoaderManager().restartLoader(0, null, (CalendarListAdapter) this.m_GridView.getAdapter());
    }

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onItemSelected(DiscordianDay mDay);
    }
}
