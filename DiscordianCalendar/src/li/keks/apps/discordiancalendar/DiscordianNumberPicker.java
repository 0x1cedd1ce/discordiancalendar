package li.keks.apps.discordiancalendar;

import android.content.Context;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.NumberKeyListener;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

/**
 * Created by ich on 09.03.14.
 */
public class DiscordianNumberPicker extends NumberPicker {

    /**
     * The text for showing the current value.
     */
    protected final EditText mInputText;


    protected SetSelectionCommand mSetSelectionCommand;

    private static final char[] DIGIT_CHARACTERS = new char[]{
            // Latin digits are the common case
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            // Arabic-Indic
            '\u0660', '\u0661', '\u0662', '\u0663', '\u0664', '\u0665', '\u0666', '\u0667', '\u0668'
            , '\u0669',
            // Extended Arabic-Indic
            '\u06f0', '\u06f1', '\u06f2', '\u06f3', '\u06f4', '\u06f5', '\u06f6', '\u06f7', '\u06f8'
            , '\u06f9',
            // minus sign
            '-'
    };


    private int getSelectedPos(String value) {
        String[] mDisplayedValues = getDisplayedValues();
        if (mDisplayedValues == null) {
            try {
                return Integer.parseInt(value);
            } catch (NumberFormatException e) {
                // Ignore as if it's not a number we don't care
            }
        } else {
            for (int i = 0; i < mDisplayedValues.length; i++) {
                // Don't force the user to type in jan when ja will do
                value = value.toLowerCase();
                if (mDisplayedValues[i].toLowerCase().startsWith(value)) {
                    return getMinValue() + i;
                }
            }


            try {
                return Integer.parseInt(value);
            } catch (NumberFormatException e) {

                // Ignore as if it's not a number we don't care
            }
        }
        return getMinValue();
    }

    class SetSelectionCommand implements Runnable {
        public int mSelectionStart;

        public int mSelectionEnd;

        public void run() {
            mInputText.setSelection(mSelectionStart, mSelectionEnd);
        }
    }

    private void postSetSelectionCommand(int selectionStart, int selectionEnd) {
        if (mSetSelectionCommand == null) {
            mSetSelectionCommand = new SetSelectionCommand();
        } else {
            removeCallbacks(mSetSelectionCommand);
        }
        mSetSelectionCommand.mSelectionStart = selectionStart;
        mSetSelectionCommand.mSelectionEnd = selectionEnd;
        post(mSetSelectionCommand);
    }

    class InputTextFilter extends NumberKeyListener {

        // XXX This doesn't allow for range limits when controlled by a
        // soft input method!
        public int getInputType() {
            return InputType.TYPE_CLASS_TEXT;
        }

        @Override
        protected char[] getAcceptedChars() {
            return DIGIT_CHARACTERS;
        }

        @Override
        public CharSequence filter(
                CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            if (getDisplayedValues() == null) {
                CharSequence f = super.filter(source, start, end, dest, dstart, dend);
                if (f == null) {
                    f = source.subSequence(start, end);
                }
                String filtered;
                if (String.valueOf(dest.subSequence(0, dstart)).equals("") &&
                        f.length() > 0 && f.charAt(0) == '-') {
                    filtered = "-" + f.toString().replace("-", "");
                } else {
                    filtered = f.toString().replace("-", "");
                }
                String result = String.valueOf(dest.subSequence(0, dstart)) + filtered
                        + dest.subSequence(dend, dest.length());
                if ("".equals(result)) {
                    return result;
                }
                int val = getSelectedPos(result);

                if (val > getMaxValue() ||
                        (result.length() > String.valueOf(getMaxValue()).length() && !result.contains("-")) ||
                        (result.length() > String.valueOf(getMaxValue()).length() + 1 && result.contains("-"))) {
                    return "";
                } else {
                    return filtered;
                }
            } else {
                CharSequence filtered = String.valueOf(source.subSequence(start, end));
                if (TextUtils.isEmpty(filtered)) {
                    return "";
                }
                String r = String.valueOf(dest.subSequence(0, dstart)) + filtered
                        + dest.subSequence(dend, dest.length());
                String result;
                if (r.startsWith("-")) {
                    result = "-" + r.replace("-", "");
                } else {
                    result = r.replace("-", "");
                }
                String str = String.valueOf(result).toLowerCase();
                for (String val : getDisplayedValues()) {
                    String valLowerCase = val.toLowerCase();
                    if (valLowerCase.startsWith(str)) {
                        postSetSelectionCommand(result.length(), val.length());
                        return val.subSequence(dstart, val.length());
                    }
                }
                return "";
            }
        }
    }

    private static final int BIAS = 1000;

    public DiscordianNumberPicker(Context context) {
        super(context);
        setMinValue(0);
        setMaxValue(2 * BIAS - 1);
        setWrapSelectorWheel(false);
        setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int i) {
                int j = i - BIAS;
                if (j > 0) {
                    return j + " minutes before the event starts";
                } else if (j < 0) {
                    return j + " minutes after the event starts";
                } else {
                    return "at the exact moment the event starts";
                }
            }
        });
        EditText input = null;
        for (int i = 0; i < this.getChildCount(); i++) {
            if (EditText.class.isInstance(this.getChildAt(i))) {
                input = (EditText) getChildAt(i);
                break;
            }
        }
        mInputText = input;
        mInputText.setOnFocusChangeListener(new OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    mInputText.setText("" + (DiscordianNumberPicker.this.getValue() - BIAS));
                    mInputText.selectAll();
                } else {
                    mInputText.setSelection(0, 0);
                    String str = String.valueOf(((TextView) v).getText());
                    int current = DiscordianNumberPicker.this.getSelectedPos(str.toString()) + BIAS;
                    setValue(current);
                }
            }
        });
        mInputText.setFilters(new InputFilter[]{
                new InputTextFilter()
        });
        setValue(BIAS);
    }

    public int getUnbiasedValue() {
        return getValue() - BIAS;
    }

    @Override
    public void setValue(int value) {
        super.setValue(value);
    }

    @Override
    public int getValue() {
        return super.getValue();
    }

    @Override
    public int getMinValue() {
        return super.getMinValue();
    }

    @Override
    public void setMinValue(int minValue) {
        super.setMinValue(minValue);
    }

    @Override
    public int getMaxValue() {
        return super.getMaxValue();
    }

    @Override
    public void setMaxValue(int maxValue) {
        super.setMaxValue(maxValue);
    }
}
