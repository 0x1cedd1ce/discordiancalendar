package li.keks.apps.discordiancalendar;

import android.database.Cursor;
import android.provider.CalendarContract;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import li.keks.apps.discordiancalendar.calendar.DiscordianDay;

/**
 * Created by oxi on 21.09.13.
 */
public class Appointment implements Comparable<Object>, Serializable {

    /**
     * arguments
     */
    public static final String arguments = "";
    /**
     * projection for the appointments
     */
    public static final String[] projection = {
            CalendarContract.Instances._ID, // 0
            CalendarContract.Instances.EVENT_ID, // 1
            CalendarContract.Instances.BEGIN, // 2
            CalendarContract.Instances.START_DAY, // 3
            CalendarContract.Instances.END, // 4
            CalendarContract.Instances.END_DAY, // 5
            CalendarContract.Instances.ALL_DAY, // 6
            CalendarContract.Instances.TITLE, // 7
            CalendarContract.Instances.CALENDAR_DISPLAY_NAME, // 8
            CalendarContract.Instances.CALENDAR_COLOR, // 9
            CalendarContract.Instances.CALENDAR_ID, // 10
            CalendarContract.Instances.DESCRIPTION, // 11
            CalendarContract.Instances.EVENT_LOCATION, // 12
            CalendarContract.Instances.CALENDAR_TIME_ZONE, // 13
            CalendarContract.Instances.EVENT_TIMEZONE, // 14
            CalendarContract.Instances.EVENT_END_TIMEZONE // 15
    };
    public static final int PROJECTION_ID = 0;
    public static final int PROJECTION_EVENT_ID = 1;
    public static final int PROJECTION_BEGIN = 2;
    public static final int PROJECTION_START_DAY = 3;
    public static final int PROJECTION_END = 4;
    public static final int PROJECTION_END_DAY = 5;
    public static final int PROJECTION_ALL_DAY = 6;
    public static final int PROJECTION_TITLE = 7;
    public static final int PROJECTION_CALENDAR_DISPLAY_NAME = 8;
    public static final int PROJECTION_CALENDAR_COLOR = 9;
    public static final int PROJECTION_CALENDAR_ID = 10;
    public static final int PROJECTION_DESCRIPTION = 11;
    public static final int PROJECTION_EVENT_LOCATION = 12;
    public static final int PROJECTION_CALENDAR_TIME_ZONE = 13;
    public static final int PROJECTION_EVENT_TIMEZONE = 14;
    public static final int PROJECTION_EVENT_END_TIMEZONE = 15;
    /**
     * selection (empty)
     */
    public static final String selection = "";
    /**
     * selection arguments (empty)
     */
    public static final String[] selection_arguments = new String[]{};
    /**
     * all day long
     */
    private final Boolean m_AllDay;
    /**
     * Name of the Calendar
     */
    private final String m_CalendarName;
    /**
     * Description
     */
    private final String m_Description;
    /**
     * end date
     */
    private final DiscordianDay m_EndDay;
    /**
     * Location
     */
    private final String m_Location;
    /**
     * start date
     */
    private final DiscordianDay m_StartDay;
    /**
     * Title
     */
    private final String m_Title;
    /**
     * event ID
     */
    private final long m_eventID;
    /**
     * instance id
     */
    private final long m_instanceID;
    /**
     * color of the calendar
     */
    private final int m_nColor;
    /**
     * id of the calendar
     */
    private final long m_CalendarId;

    /**
     * Constructor for Appointments
     *
     * @param data references a row with the {@code projection}
     */

    public Appointment(Cursor data) {
        //CalendarContract.Instances._ID, // 0
        this.m_instanceID = data.getLong(PROJECTION_ID);
        //CalendarContract.Instances.EVENT_ID, // 1
        this.m_eventID = data.getLong(PROJECTION_EVENT_ID);
        this.m_AllDay = data.getInt(PROJECTION_ALL_DAY) != 0;
        //CalendarContract.Instances.BEGIN, // 2
        long beginVal = data.getLong(PROJECTION_BEGIN);
        long endVal = data.getLong(PROJECTION_END);
        TimeZone tz = TimeZone.getTimeZone(data.getString(PROJECTION_EVENT_TIMEZONE));
        TimeZone tz_default = TimeZone.getDefault();
        beginVal -= tz_default.getOffset(beginVal);
        beginVal += tz.getOffset(beginVal);
        endVal -= tz_default.getOffset(endVal);
        endVal += tz.getOffset(endVal);
        if (m_AllDay) {
            beginVal += 1;
            endVal -= 1;
        }
        Calendar date = new GregorianCalendar();
        date.setTimeInMillis(beginVal);
        this.m_StartDay = new DiscordianDay(date.getTime());
        //CalendarContract.Instances.START_DAY, // 3
        //CalendarContract.Instances.END, // 4
        date = new GregorianCalendar();
        date.setTimeInMillis(endVal);
        this.m_EndDay = new DiscordianDay(date.getTime());
        //CalendarContract.Instances.END_DAY, // 5
        //CalendarContract.Instances.ALL_DAY, // 6
        //CalendarContract.Instances.TITLE, // 7
        this.m_Title = data.getString(PROJECTION_TITLE);
        //CalendarContract.Instances.CALENDAR_DISPLAY_NAME, // 8
        this.m_CalendarName = data.getString(PROJECTION_CALENDAR_DISPLAY_NAME);
        this.m_CalendarId = data.getLong(PROJECTION_CALENDAR_ID);
        //CalendarContract.Instances.CALENDAR_COLOR, // 9
        this.m_nColor = data.getInt(PROJECTION_CALENDAR_COLOR);
        //CalendarContract.Instances.DESCRIPTION, // 10
        this.m_Description = data.getString(PROJECTION_DESCRIPTION);
        //CalendarContract.Instances.EVENT_LOCATION // 11
        this.m_Location = data.getString(PROJECTION_EVENT_LOCATION);
    }

    /**
     * Compares this object to the specified object to determine their relative
     * order.
     *
     * @param another the object to compare to this instance.
     * @return a negative integer if this instance is less than {@code another};
     * a positive integer if this instance is greater than
     * {@code another}; 0 if this instance has the same order as
     * {@code another}.
     * @throws ClassCastException if {@code another} cannot be converted into something
     *                            comparable to {@code this} instance.
     */
    @Override
    public int compareTo(Object another) {
        if (another instanceof Appointment) {
            if (m_StartDay.compareTo(((Appointment) another).getStartDay()) >= 0) {
                if (m_StartDay.compareTo(((Appointment) another).getEndDay()) <= 0) {
                    return 0;
                } else {
                    return 1;
                }
            } else {
                if (m_EndDay.compareTo(((Appointment) another).getStartDay()) >= 1) {
                    return 0;
                } else {
                    return -1;
                }
            }
        } else if (another instanceof DiscordianDay) {
            if (m_StartDay.compareTo((DiscordianDay) another) > 0) {
                return 1;
            } else {
                if (m_EndDay.compareTo((DiscordianDay) another) >= 0) {
                    return 0;
                } else {
                    return -1;
                }
            }
        } else if (another instanceof Date) {
            if (m_StartDay.getGregorianDate().compareTo((Date) another) > 0) {
                return 1;
            } else {
                if (m_EndDay.getGregorianDate().compareTo((Date) another) >= 0) {
                    return 0;
                } else {
                    return -1;
                }
            }
        } else if (another instanceof Integer) {
            if (m_AllDay) {
                return 0;
            }
            if (m_EndDay.getHour() < (Integer) another) {
                return -1;
            }
            if (m_StartDay.getHour() > (Integer) another) {
                return 1;
            }
            return 0;
        } else {
            throw new ClassCastException();
        }
    }

    public Boolean getAllDay() {
        return m_AllDay;
    }

    public String getCalendarName() {
        return m_CalendarName;
    }

    public long getCalendarId() {
        return m_CalendarId;
    }

    public int getColor() {
        return m_nColor;
    }

    public String getDescription() {
        return m_Description;
    }

    public DiscordianDay getEndDay() {
        return m_EndDay;
    }

    public long getEventID() {
        return m_eventID;
    }

    public long getInstanceID() {
        return m_instanceID;
    }

    public String getLocation() {
        return m_Location;
    }

    public DiscordianDay getStartDay() {
        return m_StartDay;
    }

    public String getTitle() {
        return m_Title;
    }

    public String toString() {
        return m_Title;
    }

}
