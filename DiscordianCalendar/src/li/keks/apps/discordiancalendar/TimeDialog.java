package li.keks.apps.discordiancalendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

public class TimeDialog extends DialogFragment {

    public TimeDialog(boolean begin) {
        super();
        this.begin = begin;
    }

    public interface TimeDialogListener {
        public abstract void onTimeDialogClick(TimeDialog dialog, int hour, int minute, boolean begin);
    }

    private TimeDialogListener mListener;
    private int hour;
    private int minute;
    private boolean begin;
    private ClockPickerView m_clock_view;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (TimeDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        assert (getActivity() != null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        LinearLayout rootView = (LinearLayout) inflater.inflate(R.layout.time_dialog, null);

        m_clock_view = (ClockPickerView) rootView.findViewById(R.id.clock_view);
        builder.setView(rootView);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                mListener.onTimeDialogClick(TimeDialog.this, m_clock_view.getHour(), m_clock_view.getMinute(), begin);
                TimeDialog.this.dismiss();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                TimeDialog.this.dismiss();
            }
        });
        return builder.create();
    }
}
