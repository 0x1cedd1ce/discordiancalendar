package li.keks.apps.discordiancalendar;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

/**
 * Created by ich on 27.04.14.
 */
public class CalendarObserver extends ContentObserver {

    private CalendarObserverCallback mCallback;

    public CalendarObserver(Handler handler, CalendarObserverCallback callback) {
        super(handler);
        mCallback = callback;
    }

    @Override
    public void onChange(boolean selfChange) {
        if (Build.VERSION.SDK_INT >= 16) {
            onChange(selfChange, null);
        } else {
            super.onChange(selfChange);
        }
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        Log.i("CalendarObserver", "" + mCallback);
        mCallback.onChange();
    }
}
