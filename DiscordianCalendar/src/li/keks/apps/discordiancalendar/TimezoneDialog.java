package li.keks.apps.discordiancalendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class TimezoneDialog extends DialogFragment implements SearchView.OnQueryTextListener {

    public static final String TIMEZONE_ARG = "timzone_arg";
    private static String[] TimeZones;
    private static String[] TimeZoneIDs;
    private List<String> mTimeZonesVisible;

    static {
        TimeZoneIDs = TimeZone.getAvailableIDs();
        TimeZones = new String[TimeZoneIDs.length];
        for (int i = 0; i < TimeZones.length; i++) {
            TimeZones[i] = TimeZone.getTimeZone(TimeZoneIDs[i]).getDisplayName(Locale.getDefault());
        }
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        mListAdapter.clear();
        mTimeZonesVisible.clear();
        for (int i = 0; i < TimeZones.length; i++) {
            if (TimeZoneIDs[i].contains(query) || TimeZones[i].contains(query)) {
                mListAdapter.add(TimeZoneIDs[i] + " - " + TimeZones[i]);
                mTimeZonesVisible.add(TimeZoneIDs[i]);
            }
        }
        return true;
    }

    public interface TimezoneDialogListener {
        public abstract void onTimezoneDialogClick(DialogFragment dialog, TimeZone tz);
    }

    private TimezoneDialogListener mListener;
    private TimeZone mTZ;
    private ListView mListView;
    private ArrayAdapter<String> mListAdapter;

    public TimezoneDialog(TimeZone tz) {
        mTZ = tz;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (TimezoneDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        assert (getActivity() != null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View rootView = inflater.inflate(R.layout.timezone_dialog, null);
        SearchView searchView = (SearchView) rootView.findViewById(R.id.searchView);
        mListView = (ListView) rootView.findViewById(R.id.listView);
        mListAdapter = new ArrayAdapter<String>(getActivity(), R.layout.timezone_item, android.R.id.text1);
        mTimeZonesVisible = new ArrayList<String>();
        mListView.setAdapter(mListAdapter);
        searchView.setOnQueryTextListener(this);
        builder.setView(rootView);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (mListView.isSelected()) {
                    mTZ = TimeZone.getTimeZone(mTimeZonesVisible.get(mListView.getSelectedItemPosition()));
                    mListener.onTimezoneDialogClick(TimezoneDialog.this, mTZ);
                    TimezoneDialog.this.dismiss();
                }
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                TimezoneDialog.this.dismiss();
            }
        });
        return builder.create();
    }


}
