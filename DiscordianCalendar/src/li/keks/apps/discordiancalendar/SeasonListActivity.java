package li.keks.apps.discordiancalendar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import li.keks.apps.discordiancalendar.calendar.DiscordianDay;

/**
 * An activity representing a list of Seasons. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link DayListActivity} representing item details. On tablets, the
 * activity presents the list of items and item details side-by-side using two
 * vertical panes.
 * <p/>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link SeasonListFragment} and the item details (if present) is a
 * {@link DayListFragment}.
 * <p/>
 * This activity also implements the required
 * {@link SeasonListFragment.Callbacks} interface to listen for item selections.
 */
public class SeasonListActivity extends Activity implements
        SeasonListFragment.Callbacks {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    /**
     * @param savedInstanceState contains the saved data from last instance
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_season_list);

        if (findViewById(R.id.season_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;

            // In two-pane mode, list items should be given the
            // 'activated' state when touched.
            if (((SeasonListFragment) getFragmentManager().findFragmentById(
                    R.id.season_list)) != null) {
                ((SeasonListFragment) getFragmentManager().findFragmentById(
                        R.id.season_list)).setActivateOnItemClick(true);
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            ((SeasonListFragment) getFragmentManager().findFragmentById(
                    R.id.season_list)).onChange();
        }
    }


    /**
     * Callback method from {@link SeasonListFragment.Callbacks} indicating that
     * the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(DiscordianDay mDay) {
        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putSerializable(DayListFragment.ARG_ITEM_ID, mDay);
            DayListFragment fragment = new DayListFragment();
            fragment.setArguments(arguments);
            getFragmentManager().beginTransaction()
                    .add(R.id.season_detail_container, fragment).commit();

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(this, DayListActivity.class);
            detailIntent.putExtra(DayListFragment.ARG_ITEM_ID, mDay);
            startActivityForResult(detailIntent, 0);
        }
    }
}
