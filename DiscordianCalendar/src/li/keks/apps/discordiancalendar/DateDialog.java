package li.keks.apps.discordiancalendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.graphics.Color;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import li.keks.apps.discordiancalendar.calendar.DiscordianDay;
import li.keks.apps.discordiancalendar.calendar.Season;

public class DateDialog extends DialogFragment implements GridView.OnItemClickListener,
        GestureOverlayView.OnGesturePerformedListener {

    public static final String DATE_ARG = "date_arg";
    private DateDialogListener mListener;
    private DiscordianDay day;
    private boolean begin;
    private GridView m_GridView;
    private GestureLibrary gestureLib;
    private Season.ESeason m_Season;
    private int m_Year;

    public DateDialog(DiscordianDay day, boolean begin) {
        super();
        this.begin = begin;
        this.day = day;
        this.m_Year = day.getYear();
        this.m_Season = day.getSeason();
    }

    @Override
    public void onGesturePerformed(GestureOverlayView gestureOverlayView, Gesture gesture) {
        ArrayList<Prediction> predictions = gestureLib.recognize(gesture);
        for (Prediction prediction : predictions) {
            if (prediction.score > 1.0) {
                if (prediction.name.equals("next")) {
                    if (this.m_Season == Season.ESeason.THE_AFTERMATH) {
                        this.m_Year++;
                    }
                    this.setSeason(this.m_Season.increment(), this.m_Year);
                } else if (prediction.name.equals("prev")) {
                    if (this.m_Season == Season.ESeason.CHAOS) {
                        this.m_Year--;
                    }
                    this.setSeason(this.m_Season.decrement(), this.m_Year);
                }
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ((CalendarListAdapter) m_GridView.getAdapter()).selectItem(position);
        Resources res = view.getResources();
        Drawable shape = res.getDrawable(R.drawable.frame);
        float[] mat = new float[20];
        mat[0] = 0x4f;
        mat[5] = 0xa5;
        mat[10] = 0xd5;
        mat[19] = 0xff;
        mat[1] = Color.red(Color.GREEN);
        mat[6] = Color.green(Color.GREEN);
        mat[11] = Color.blue(Color.GREEN);
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(mat);
        shape.setColorFilter(filter);
        if (Build.VERSION.SDK_INT >= 16) {
            view.setBackground(shape);
        } else {
            view.setBackgroundDrawable(shape);
        }
        this.day = (DiscordianDay) m_GridView.getAdapter().getItem(position);
    }

    public interface DateDialogListener {
        public abstract void onDateDialogClick(DateDialog dialog, DiscordianDay day, boolean begin);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (DateDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement DateDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        assert (getActivity() != null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // retrieve display dimensions
        Rect displayRectangle = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        LinearLayout rootView = (LinearLayout) inflater.inflate(R.layout.date_dialog, null);
        rootView.setMinimumHeight((int) (displayRectangle.height() * 0.9f));

        this.m_GridView = (GridView) rootView.findViewById(R.id.gridView);
        this.m_GridView.setOnItemClickListener(this);
        this.m_GridView.setMinimumHeight((int) (displayRectangle.height() * 0.9f));
        setSeason(m_Season, m_Year);

        GestureOverlayView gestureOverlayView = new GestureOverlayView(this.getActivity());
        gestureOverlayView.addView(rootView);
        gestureOverlayView.setMinimumHeight((int) (displayRectangle.height() * 0.9f));
        gestureOverlayView.addOnGesturePerformedListener(this);
        gestureLib = GestureLibraries.fromRawResource(this.getActivity(), R.raw.gestures);
        if (!gestureLib.load()) {
            this.getActivity().finish();
        }

        builder.setView(gestureOverlayView);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                mListener.onDateDialogClick(DateDialog.this, day, begin);
                DateDialog.this.dismiss();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DateDialog.this.dismiss();
            }
        });
        return builder.create();
    }

    private void setSeason(Season.ESeason eSeason, int nYear) {
        this.m_Season = eSeason;
        this.m_Year = nYear;
        CalendarListAdapter listAdapter =
                new CalendarListAdapter(this.getActivity(), this.m_Season, this.m_Year);
        Rect displayRectangle = new Rect();
        getActivity().getWindowManager().getDefaultDisplay().getRectSize(displayRectangle);
        //getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        listAdapter.setQuadratic(false, displayRectangle.height() * 0.8f);
        this.m_GridView.setAdapter(listAdapter);
        getLoaderManager().restartLoader(0, null, listAdapter);
    }

}
