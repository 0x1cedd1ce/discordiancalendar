package li.keks.apps.discordiancalendar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import li.keks.apps.discordiancalendar.calendar.DiscordianDay;

/**
 * Created by oxi on 03.11.13.
 */
public class TimeBar extends View {

    private final int[] m_Colors = new int[48];
    private final List<Integer> m_AllDays = new ArrayList<Integer>(Color.TRANSPARENT);
    private DiscordianDay m_currentDay;

    public TimeBar(Context context, DiscordianDay currentDay) {
        super(context);
        this.m_currentDay = currentDay;
    }

    public void setAppointments(List<Appointment> lAppointments) {
        for (int i = 0; i < 48; i++) {
            m_Colors[i] = Color.WHITE;
        }
        m_AllDays.clear();
        for (Appointment itApp : lAppointments) {
            if (itApp.getAllDay()) {
                m_AllDays.add(itApp.getColor());
            } else {
                int i = itApp.getStartDay().getHour() * 2 + itApp.getStartDay().getMinute() / 60;
                if (itApp.getStartDay().getDay() < m_currentDay.getDay()) {
                    i = 0;
                }
                int nEnd = itApp.getEndDay().getHour() * 2 + itApp.getEndDay().getMinute() / 60;
                if (itApp.getEndDay().getDay() > m_currentDay.getDay()) {
                    nEnd = 47;
                }
                try {
                    for (; i < nEnd; i++) {
                        m_Colors[i] = itApp.getColor();
                    }
                } catch (Exception e) {
                    Log.i("TimeBar", "Index out of range");
                }
            }
        }
    }

    protected void onDraw(Canvas canvas) {
        float chunkHeight = (float) canvas.getHeight() / (float) 48;
        float chunkWidth = (float) canvas.getWidth() / (float) 5;
        for (int i = 0; i < 48; i++) {
            Paint paint = new Paint();
            paint.setColor(m_Colors[i]);
            canvas.drawRect(
                    chunkWidth * 0,
                    (i) * chunkHeight, chunkWidth * 1, (i + 1) * chunkHeight, paint);
        }
        int index = 1;
        for (int color : m_AllDays) {
            Paint paint = new Paint();
            paint.setColor(color);
            canvas.drawRect(
                    chunkWidth * index,
                    48 * chunkHeight, chunkWidth * (index + 1), 48 * chunkHeight, paint);
        }
        canvas.save();
    }
}
