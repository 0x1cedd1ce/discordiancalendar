package li.keks.apps.discordiancalendar;

import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;

import li.keks.apps.discordiancalendar.calendar.DiscordianDay;
import li.keks.apps.discordiancalendar.calendar.Season;

/**
 * Created by oxi on 01.09.13.
 */
public class CalendarListAdapter extends BaseAdapter implements ListAdapter,
        LoaderManager.LoaderCallbacks<Cursor> {

    /**
     * context of this adapter
     */
    private Context m_Context;
    /**
     * uri for the loader
     */
    private Uri m_LoaderUri;
    /**
     * the current season
     */
    private Season.ESeason m_eSeason;
    /**
     * All appointment of this season stored as a list
     */
    private ArrayList<Appointment> m_lAppointments;
    /**
     * the amount of days in this loader
     */
    private int m_nCount;
    /**
     * the days shown of the previous season
     */
    private int m_nPaddingDays;
    /**
     * the current year
     */
    private int m_nYear;
    /**
     * is true if the current year is a leap year in gregorian calendar
     */
    private boolean m_bLeapYear;
    /**
     * if true a quadratic layout is used for the item views
     */
    private boolean m_bQuadratic;
    private int m_minHeight;
    private int m_selected = -1;

    public void selectItem(int position) {
        m_selected = position;
    }

    public void setQuadratic(boolean bQuadratic, double maxHeight) {
        this.m_bQuadratic = bQuadratic;
        m_minHeight = (int) (maxHeight / (m_nCount / 5));
    }

    /**
     * @param context The context of this Adapter. Needed for loader and content resolver
     * @param eSeason The Discordian Season
     * @param nYear   The Year Of the Lady Discord
     */

    public CalendarListAdapter(Context context, Season.ESeason eSeason, int nYear) {
        this.m_bQuadratic = true;
        this.m_Context = context;
        this.m_eSeason = eSeason;
        this.m_nYear = nYear;
        this.m_lAppointments = new ArrayList<Appointment>();
        GregorianCalendar beginTime = new GregorianCalendar();
        beginTime.set(Calendar.AM_PM, Calendar.AM);
        GregorianCalendar endTime;
        m_bLeapYear = beginTime.isLeapYear(this.m_nYear - 1166);
        switch (this.m_eSeason) {
            case ST_TIBS_DAY:
            case CHAOS:
                beginTime =
                        new DiscordianDay(1, Season.ESeason.CHAOS, this.m_nYear).getGregorianDay();
                if (m_bLeapYear) {
                    endTime =
                            new DiscordianDay(1, Season.ESeason.DISCORD, this.m_nYear).getGregorianDay();
                    this.m_nCount = 80;
                } else {
                    endTime =
                            new DiscordianDay(2, Season.ESeason.DISCORD, this.m_nYear).getGregorianDay();
                    this.m_nCount = 75;
                }
                this.m_nPaddingDays = 0;
                break;
            case DISCORD:
                beginTime =
                        new DiscordianDay(71, Season.ESeason.CHAOS, this.m_nYear).getGregorianDay();
                endTime =
                        new DiscordianDay(4, Season.ESeason.CONFUSION, this.m_nYear).getGregorianDay();
                this.m_nCount = 80;
                this.m_nPaddingDays = 3;
                break;
            case CONFUSION:
                beginTime =
                        new DiscordianDay(73, Season.ESeason.DISCORD, this.m_nYear).getGregorianDay();
                endTime =
                        new DiscordianDay(2, Season.ESeason.BUREAUCRACY, this.m_nYear).getGregorianDay();
                this.m_nCount = 75;
                this.m_nPaddingDays = 1;
                break;
            case BUREAUCRACY:
                beginTime =
                        new DiscordianDay(70, Season.ESeason.CONFUSION, this.m_nYear).getGregorianDay();
                endTime =
                        new DiscordianDay(3, Season.ESeason.THE_AFTERMATH, this.m_nYear).getGregorianDay();
                this.m_nCount = 80;
                this.m_nPaddingDays = 4;
                break;
            case THE_AFTERMATH:
                beginTime =
                        new DiscordianDay(72, Season.ESeason.BUREAUCRACY, this.m_nYear).getGregorianDay();
                endTime =
                        new DiscordianDay(73, Season.ESeason.THE_AFTERMATH, this.m_nYear).getGregorianDay();
                this.m_nCount = 75;
                this.m_nPaddingDays = 2;
                break;
            //case ST_TIBS_DAY:
            //throw new StTibsException("Woha, Wait a minute_png! St. Tibs Day? Better not leave the house today.");
            //    break;
            default:
                throw new RuntimeException("This should never be reached! Seriously!");
        }

        beginTime.set(Calendar.HOUR_OF_DAY, 0);
        beginTime.set(Calendar.MINUTE, 1);
        long startMillis = beginTime.getTimeInMillis();

        endTime.set(Calendar.HOUR_OF_DAY, 23);
        endTime.set(Calendar.MINUTE, 59);
        long endMillis = endTime.getTimeInMillis();

        Uri.Builder builder = CalendarContract.Instances.CONTENT_URI.buildUpon();
        ContentUris.appendId(builder, startMillis);
        ContentUris.appendId(builder, endMillis);

        this.m_LoaderUri = builder.build();
    }

    private List<Appointment> getAppointments(DiscordianDay dDay) {
        List<Appointment> result = new ArrayList<Appointment>();
        for (Appointment app : this.m_lAppointments) {
            if (app.compareTo(dDay) == 0) {
                result.add(app);
            }
        }
        return result;
    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        return this.m_nCount;
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        if (position < m_nPaddingDays) {
            return new DiscordianDay(
                    73 - m_nPaddingDays + position + 1, this.m_eSeason.decrement(), this.m_nYear);
        } else if (position + 1 - m_nPaddingDays > 73) {
            return new DiscordianDay(
                    position + 1 - m_nPaddingDays - 73, this.m_eSeason.increment(), this.m_nYear);
        }
        if (m_bLeapYear && this.m_eSeason == Season.ESeason.CHAOS) {
            if (position >= 28 && position <= 32) {
                return new DiscordianDay(29, Season.ESeason.ST_TIBS_DAY, this.m_nYear);
            } else if (position < 28) {
                return new DiscordianDay(
                        position + 1 - m_nPaddingDays, this.m_eSeason, this.m_nYear);
            } else {
                return new DiscordianDay(
                        position - 4 - m_nPaddingDays, this.m_eSeason, this.m_nYear);
            }
        } else {
            return new DiscordianDay(position + 1 - m_nPaddingDays, this.m_eSeason, this.m_nYear);
        }
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return position + 73 * (this.m_eSeason.ordinal() + 5 * this.m_nYear);
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link android.view.LayoutInflater#inflate(int, android.view.ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to
     *                    convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DiscordianDay currentDay = (DiscordianDay) this.getItem(position);
        LinearLayout ret;
        if (m_bQuadratic) {
            ret = new SquareLinearLayout(parent.getContext());
        } else {
            ret = new LinearLayout(parent.getContext());
            ret.setMinimumHeight(m_minHeight);
        }
        TextView number = new TextView(this.m_Context);
        number.setTypeface(Typeface.MONOSPACE);
        number.setGravity(Gravity.RIGHT);
        if (currentDay.getDay() <= 9) {
            number.setText(" " + currentDay.getDay());
        } else {
            number.setText("" + currentDay.getDay());
        }
        number.setPadding(10, 0, 10, 0);
        ret.addView(number);
        //ListView appList = new ListView(this.m_Context);
        List<Appointment> appointments = getAppointments(currentDay);
        TimeBar timeBar = new TimeBar(this.m_Context, currentDay);
        //if(!m_bQuadratic) {
        //timeBar.setMinimumHeight(100);
        //}
        ViewGroup.LayoutParams params = timeBar.getLayoutParams();
        if (params == null) {
            params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        timeBar.setLayoutParams(params);
        timeBar.setAppointments(appointments);
        ret.addView(timeBar);
        Resources res = ret.getResources();
        Drawable shape = res.getDrawable(R.drawable.frame);
        float[] mat = new float[20];
        mat[0] = 0x4f;
        mat[5] = 0xa5;
        mat[10] = 0xd5;

        mat[19] = 0xff;
        if (currentDay.getSeason() != this.m_eSeason) {
            if (currentDay.getSeason() == Season.ESeason.ST_TIBS_DAY) {
                mat[1] = Color.red(Color.RED);
                mat[6] = Color.green(Color.RED);
                mat[11] = Color.blue(Color.RED);
                switch (position) {
                    case 30:
                        number.setText("St.");
                        ret.removeView(timeBar);
                        break;
                    case 31:
                        number.setText("Tibs");
                        ret.removeView(timeBar);
                        break;
                    case 32:
                        number.setText("Day");
                        ret.removeView(timeBar);
                        break;
                    default:
                        number.setText("  ");
                }
            } else {
                mat[1] = Color.red(Color.GRAY);
                mat[6] = Color.green(Color.GRAY);
                mat[11] = Color.blue(Color.GRAY);
            }
        } else {
            if (m_selected != -1 && position == m_selected) {
                mat[1] = Color.red(Color.GREEN);
                mat[6] = Color.green(Color.GREEN);
                mat[11] = Color.blue(Color.GREEN);
            } else if (currentDay.isToday()) {
                mat[1] = Color.red(Color.YELLOW);
                mat[6] = Color.green(Color.YELLOW);
                mat[11] = Color.blue(Color.YELLOW);
            } else {
                mat[1] = Color.red(Color.LTGRAY);
                mat[6] = Color.green(Color.LTGRAY);
                mat[11] = Color.blue(Color.LTGRAY);
            }
        }
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(mat);
        shape.setColorFilter(filter);
        if (Build.VERSION.SDK_INT >= 16) {
            ret.setBackground(shape);
        } else {
            ret.setBackgroundDrawable(shape);
        }
        //appList.setAdapter(new ArrayAdapter<Appointment>(this.m_Context, R.layout.appointment, appointments));
        //ret.addView(appList);
        return ret;
    }

    /**
     * Instantiate and return a new Loader for the given ID.
     *
     * @param id   The ID whose loader is to be created.
     * @param args Any arguments supplied by the caller.
     * @return Return a new Loader instance that is ready to start loading.
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        this.m_lAppointments.clear();

        return new CursorLoader(this.m_Context, this.m_LoaderUri, Appointment.projection,
                Appointment.selection, Appointment.selection_arguments, Appointment.arguments);
    }

    /**
     * Called when a previously created loader has finished its load.  Note
     * that normally an application is <em>not</em> allowed to commit fragment
     * transactions while in this call, since it can happen after an
     * activity's state is saved.  See { FragmentManager#beginTransaction()
     * FragmentManager.openTransaction()} for further discussion on this.
     * <p/>
     * <p>This function is guaranteed to be called prior to the release of
     * the last data that was supplied for this Loader.  At this point
     * you should remove all use of the old data (since it will be released
     * soon), but should not do your own release of the data since its Loader
     * owns it and will take care of that.  The Loader will take care of
     * management of its data so you don't have to.  In particular:
     * <p/>
     * <ul>
     * <li> <p>The Loader will monitor for changes to the data, and report
     * them to you through new calls here.  You should not monitor the
     * data yourself.  For example, if the data is a {@link android.database.Cursor}
     * and you place it in a {@link android.widget.CursorAdapter}, use
     * the {@link android.widget.CursorAdapter#CursorAdapter(android.content.Context,
     * android.database.Cursor, int)} constructor <em>without</em> passing
     * in either {@link android.widget.CursorAdapter#FLAG_AUTO_REQUERY}
     * or {@link android.widget.CursorAdapter#FLAG_REGISTER_CONTENT_OBSERVER}
     * (that is, use 0 for the flags argument).  This prevents the CursorAdapter
     * from doing its own observing of the Cursor, which is not needed since
     * when a change happens you will get a new Cursor throw another call
     * here.
     * <li> The Loader will release the data once it knows the application
     * is no longer using it.  For example, if the data is
     * a {@link android.database.Cursor} from a {@link android.content.CursorLoader},
     * you should not call close() on it yourself.  If the Cursor is being placed in a
     * {@link android.widget.CursorAdapter}, you should use the
     * {@link android.widget.CursorAdapter#swapCursor(android.database.Cursor)}
     * method so that the old Cursor is not closed.
     * </ul>
     *
     * @param loader The Loader that has finished.
     * @param data   The data generated by the Loader.
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        while (data.moveToNext()) {
            this.m_lAppointments.add(new Appointment(data));
        }
        this.m_lAppointments.trimToSize();
        Collections.sort(this.m_lAppointments);
        this.notifyDataSetChanged();
    }

    /**
     * Called when a previously created loader is being reset, and thus
     * making its data unavailable.  The application should at this point
     * remove any references it has to the Loader's data.
     *
     * @param loader The Loader that is being reset.
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        this.m_lAppointments.clear();
    }
}
