package li.keks.apps.discordiancalendar;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

/**
 * Created by oxi on 01.09.13.
 */
public class DayListAdapter extends BaseAdapter implements ListAdapter {

    /**
     * context of this adapter
     */
    private final Context m_Context;

    /**
     * @param context The context of this Adapter. Needed for loader and content resolver
     */
    public DayListAdapter(Context context) {
        this.m_Context = context;
    }

    /**
     * How many items are in the data set represented by this Adapter.
     *
     * @return Count of items.
     */
    @Override
    public int getCount() {
        return 48;
    }

    /**
     * Get the data item associated with the specified position in the data set.
     *
     * @param position Position of the item whose data we want within the adapter's
     *                 data set.
     * @return The data at the specified position.
     */
    @Override
    public Object getItem(int position) {
        return position;
    }

    /**
     * Get the row id associated with the specified position in the list.
     *
     * @param position The position of the item within the adapter's data set whose row id we want.
     * @return The id of the item at the specified position.
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Get a View that displays the data at the specified position in the data set. You can either
     * create a View manually or inflate it from an XML layout file. When the View is inflated, the
     * parent View (GridView, ListView...) will apply default layout parameters unless you use
     * {@link android.view.LayoutInflater#inflate(int, android.view.ViewGroup, boolean)}
     * to specify a root view and to prevent attachment to the root.
     *
     * @param position    The position of the item within the adapter's data set of the item whose view
     *                    we want.
     * @param convertView The old view to reuse, if possible. Note: You should check that this view
     *                    is non-null and of an appropriate type before using. If it is not possible to
     *                    convert
     *                    this view to display the correct data, this method can create a new view.
     *                    Heterogeneous lists can specify their number of view types, so that this View is
     *                    always of the right type (see {@link #getViewTypeCount()} and
     *                    {@link #getItemViewType(int)}).
     * @param parent      The parent that this view will eventually be attached to
     * @return A View corresponding to the data at the specified position.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout ret = new LinearLayout(m_Context);
        ret.setOrientation(LinearLayout.VERTICAL);
        TextView number = new TextView(this.m_Context);
        if (position % 2 == 0) {
            if (position / 2 <= 9) {
                number.setText(" " + position / 2);
            } else {
                number.setText(position / 2 + "");
            }
        } else {
            number.setText("  ");
        }
        number.setMinEms(1);
        number.setBackgroundResource(android.R.drawable.screen_background_dark);
        number.setTextColor(Color.LTGRAY);
        number.setTypeface(Typeface.MONOSPACE);
        ret.addView(number);
        return ret;
    }
}
