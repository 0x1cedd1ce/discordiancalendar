package li.keks.apps.discordiancalendar;

/**
 * Created by ich on 27.04.14.
 */
public interface CalendarObserverCallback {
    public void onChange();
}
