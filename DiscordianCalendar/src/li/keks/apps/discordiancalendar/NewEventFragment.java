package li.keks.apps.discordiancalendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.TimeZone;

import li.keks.apps.discordiancalendar.calendar.DiscordianDay;

public class NewEventFragment extends Fragment implements View.OnClickListener,
        TimezoneDialog.TimezoneDialogListener, TimeDialog.TimeDialogListener,
        DateDialog.DateDialogListener {

    public static final String ARG_DAY = "item_day";
    public static final String ARG_YEAR = "item_year";
    public static final String ARG_HOUR = "item_hour";
    public static final String ARG_APP = "item_app";
    public static final String PREFS_DEFAULT_CALENDAR = "default_calendar";
    public static final int PICKER_BIAS = Integer.MAX_VALUE / 2;

    private static final String[] projection = new String[]{
            CalendarContract.Calendars._ID,
            CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,
            CalendarContract.Calendars.ACCOUNT_NAME,
            CalendarContract.Calendars.CALENDAR_COLOR,
            CalendarContract.Calendars.CALENDAR_TIME_ZONE
    };

    private ArrayList<String> m_CalendarNames = new ArrayList<String>();
    private LinkedList<Integer> m_CalendarColors = new LinkedList<Integer>();
    private LinkedList<Long> m_CalendarIds = new LinkedList<Long>();
    private LinkedList<TimeZone> m_CalendarTimeZones = new LinkedList<TimeZone>();

    private Button m_ok;
    private Button m_cancel;
    private Button m_calendar_chooser;
    private EditText m_name;
    private EditText m_place;
    private Button m_timezone;
    private TimeZone m_tz;
    private View.OnClickListener m_timezone_listener;
    private Button m_begin_date;
    private Button m_begin_time;
    private Button m_end_date;
    private Button m_end_time;
    private DiscordianDay m_begin_ddate;
    private int m_begin_time_hour;
    private int m_begin_time_minute;
    private DiscordianDay m_end_ddate;
    private int m_end_time_hour;
    private int m_end_time_minute;
    private View.OnClickListener m_begin_date_listener;
    private View.OnClickListener m_begin_time_listener;
    private View.OnClickListener m_end_date_listener;
    private View.OnClickListener m_end_time_listener;
    private CheckBox m_all_day;
    private EditText m_description;
    private Button m_repeat;
    private View.OnClickListener m_repeat_listener;
    private LinearLayout m_reminder;
    private Button m_add_reminder;
    private Spinner m_status;
    private SpinnerAdapter m_status_adapter;
    private Spinner m_privacy;
    private SpinnerAdapter m_privacy_adapter;
    private DiscordianNumberPicker mPicker;
    private Appointment m_app;

    public NewEventFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(ARG_DAY)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            DiscordianDay day = (DiscordianDay) getArguments().getSerializable(ARG_DAY);
            assert day != null;
            m_begin_ddate = day;
            m_end_ddate = day;
        }
        if (getArguments().containsKey(ARG_HOUR)) {
            m_begin_time_hour = Integer.valueOf(getArguments().getString(ARG_HOUR));
            if (m_begin_time_hour == 23) {
                m_end_time_hour = 0;
                m_end_ddate = m_end_ddate.inc();
            } else {
                m_end_time_hour = m_begin_time_hour + 1;
            }
        }
        if (getArguments().containsKey(ARG_APP)) {
            Appointment app = (Appointment) getArguments().getSerializable(ARG_APP);
            m_begin_ddate = app.getStartDay();
            m_begin_time_hour = m_begin_ddate.getHour();
            m_begin_time_minute = m_begin_ddate.getMinute();
            m_end_ddate = app.getEndDay();
            m_end_time_hour = m_end_ddate.getHour();
            m_end_time_minute = m_end_ddate.getMinute();
            m_app = app;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_event, container, false);

        m_ok = (Button) rootView.findViewById(R.id.ok);
        m_ok.setOnClickListener(this);

        m_cancel = (Button) rootView.findViewById(R.id.cancel);
        m_cancel.setOnClickListener(this);

        m_calendar_chooser = (Button) rootView.findViewById(R.id.calendar_chooser);
        ContentResolver cr = getActivity().getContentResolver();
        Uri uri = CalendarContract.Calendars.CONTENT_URI;
        String selection = CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL + " >= ?";
        String[] selectionArgs = new String[]{CalendarContract.Calendars.CAL_ACCESS_CONTRIBUTOR + ""};
        Cursor cur = cr.query(uri, projection, selection, selectionArgs, null);
        while (cur.moveToNext()) {
            m_CalendarIds.add(Long.valueOf(cur.getLong(0)));
            m_CalendarNames.add(cur.getString(1) + " ( " + cur.getString(2) + " )");
            m_CalendarColors.add(Integer.valueOf(cur.getInt(3)));
            if (cur.getString(4) == null || cur.getString(4).equals("null")) {
                m_CalendarTimeZones.add(TimeZone.getDefault());
            } else {
                m_CalendarTimeZones.add(TimeZone.getTimeZone(cur.getString(4)));
            }
        }
        SharedPreferences prefs = getActivity().getPreferences(Context.MODE_PRIVATE);
        if (prefs.contains(PREFS_DEFAULT_CALENDAR)) {
            long id = prefs.getLong(PREFS_DEFAULT_CALENDAR, 0);
            for (int i = 0; i < m_CalendarIds.size(); i++) {
                if (m_CalendarIds.get(i) == id) {
                    m_calendar_chooser.setBackgroundColor(m_CalendarColors.get(i));
                    m_calendar_chooser.setText(m_CalendarNames.get(i));
                    m_calendar_chooser.setTag(m_CalendarIds.get(i));
                    m_tz = m_CalendarTimeZones.get(i);
                }
            }
        } else {
            m_calendar_chooser.setBackgroundColor(m_CalendarColors.get(0));
            m_calendar_chooser.setText(m_CalendarNames.get(0));
            m_calendar_chooser.setTag(m_CalendarIds.get(0));
            m_tz = m_CalendarTimeZones.get(0);
        }
        m_calendar_chooser.setOnClickListener(this);

        m_name = (EditText) rootView.findViewById(R.id.name);

        m_place = (EditText) rootView.findViewById(R.id.place);

        m_timezone = (Button) rootView.findViewById(R.id.timezone);

        m_timezone_listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard();
                DialogFragment dialog = new TimezoneDialog(m_tz);
                dialog.show(getFragmentManager(), "TimezoneDialog");
            }
        };
        m_timezone.setOnClickListener(m_timezone_listener);
        this.onTimezoneDialogClick(null, m_tz);

        m_begin_date = (Button) rootView.findViewById(R.id.begin_date);
        m_begin_date_listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialog = new DateDialog(m_begin_ddate, true);
                dialog.show(getFragmentManager(), "BeginDateDialog");
                hideSoftKeyboard();
            }
        };
        m_begin_date.setOnClickListener(m_begin_date_listener);
        this.onDateDialogClick(null, m_begin_ddate, true);

        m_begin_time = (Button) rootView.findViewById(R.id.begin_time);
        m_begin_time_listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard();
                DialogFragment dialog = new TimeDialog(true);
                dialog.show(getFragmentManager(), "BeginTimeDialog");
            }
        };
        m_begin_time.setOnClickListener(m_begin_time_listener);
        this.onTimeDialogClick(null, m_begin_time_hour, m_begin_time_minute, true);

        m_end_date = (Button) rootView.findViewById(R.id.end_date);
        m_end_date_listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard();
                DialogFragment dialog = new DateDialog(m_end_ddate, false);
                dialog.show(getFragmentManager(), "EndDateDialog");
            }
        };
        m_end_date.setOnClickListener(m_end_date_listener);
        this.onDateDialogClick(null, m_end_ddate, false);

        m_end_time = (Button) rootView.findViewById(R.id.end_time);
        m_end_time_listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialog = new TimeDialog(false);
                dialog.show(getFragmentManager(), "EndTimeDialog");
                hideSoftKeyboard();
            }
        };
        m_end_time.setOnClickListener(m_end_time_listener);
        this.onTimeDialogClick(null, m_end_time_hour, m_end_time_minute, false);

        m_all_day = (CheckBox) rootView.findViewById(R.id.all_day);

        m_description = (EditText) rootView.findViewById(R.id.description);

        m_repeat = (Button) rootView.findViewById(R.id.repeat);
        m_repeat_listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //DialogFragment dialog = new TimeDialog(false);
                //dialog.show(getFragmentManager(), "EndTimeDialog");
            }
        };
        m_repeat.setOnClickListener(m_repeat_listener);

        m_reminder = (LinearLayout) rootView.findViewById(R.id.reminder_list);
        addReminder(10);

        m_add_reminder = (Button) rootView.findViewById(R.id.add_reminder);
        m_add_reminder.setOnClickListener(this);

        m_status = (Spinner) rootView.findViewById(R.id.status);
        String[] data = new String[]{"Busy", "Free", "Tentative"};
        m_status_adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_dropdown_item, data);
        m_status.setAdapter(m_status_adapter);

        m_privacy = (Spinner) rootView.findViewById(R.id.privacy);
        data = new String[]{"Default", "Private", "Confidental", "Public"};
        m_privacy_adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_dropdown_item, data);
        m_privacy.setAdapter(m_privacy_adapter);

        if (m_app != null) {
            m_name.setText(m_app.getTitle());
            for (int i = 0; i < m_CalendarIds.size(); i++) {
                if (m_app.getCalendarId() == m_CalendarIds.get(i)) {
                    m_calendar_chooser.setBackgroundColor(m_CalendarColors.get(i));
                    m_calendar_chooser.setText(m_CalendarNames.get(i));
                    m_calendar_chooser.setTag(m_CalendarIds.get(i));
                    m_tz = m_CalendarTimeZones.get(i);
                    break;
                }
            }
            m_place.setText(m_app.getLocation());
            this.onTimezoneDialogClick(null, m_tz);
            m_all_day.setChecked(m_app.getAllDay());
            m_description.setText(m_app.getDescription());
            // TODO:
            //m_repeat;
            //m_reminder;
            //m_status;
            //m_privacy;
        }

        return rootView;
    }

    public void hideSoftKeyboard() {
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.ok) {
            long calID = ((Long) m_calendar_chooser.getTag()).longValue();
            SharedPreferences prefs = getActivity().getPreferences(Context.MODE_PRIVATE);
            prefs.edit().putLong(PREFS_DEFAULT_CALENDAR, calID).commit();
            long startMillis;
            long endMillis;
            GregorianCalendar startDay = m_begin_ddate.getGregorianDay();
            startDay.set(Calendar.HOUR_OF_DAY, m_begin_time_hour);
            startDay.set(Calendar.MINUTE, m_begin_time_minute);
            startDay.set(Calendar.SECOND, 0);
            startDay.set(Calendar.MILLISECOND, 0);
            startMillis = startDay.getTimeInMillis();

            GregorianCalendar endDay = m_end_ddate.getGregorianDay();
            endDay.set(Calendar.HOUR_OF_DAY, m_end_time_hour);
            endDay.set(Calendar.MINUTE, m_end_time_minute);
            endDay.set(Calendar.SECOND, 0);
            endDay.set(Calendar.MILLISECOND, 0);
            endMillis = endDay.getTimeInMillis();

            ContentResolver cr = getActivity().getContentResolver();
            ContentValues values = new ContentValues();
            values.put(CalendarContract.Events.DTSTART, startMillis);
            values.put(CalendarContract.Events.DTEND, endMillis);
            values.put(CalendarContract.Events.TITLE, m_name.getText().toString());
            values.put(CalendarContract.Events.DESCRIPTION, m_description.getText().toString());
            values.put(CalendarContract.Events.CALENDAR_ID, calID);
            values.put(CalendarContract.Events.EVENT_TIMEZONE, m_tz.getID());
            values.put(CalendarContract.Events.EVENT_LOCATION, m_place.getText().toString());

            String status = (String) m_status.getSelectedItem();
            if (status.equals("Busy")) {
                values.put(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
            } else if (status.equals("Free")) {
                values.put(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_FREE);
            } else if (status.equals("Tentative")) {
                values.put(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_TENTATIVE);
            }
            String privacy = (String) m_privacy.getSelectedItem();
            if (privacy.equals("Default")) {
                values.put(CalendarContract.Events.ACCESS_LEVEL, CalendarContract.Events.ACCESS_DEFAULT);
            } else if (privacy.equals("Private")) {
                values.put(CalendarContract.Events.ACCESS_LEVEL, CalendarContract.Events.ACCESS_PRIVATE);
            } else if (privacy.equals("Confidental")) {
                values.put(CalendarContract.Events.ACCESS_LEVEL, CalendarContract.Events.ACCESS_CONFIDENTIAL);
            } else if (privacy.equals("Public")) {
                values.put(CalendarContract.Events.ACCESS_LEVEL, CalendarContract.Events.ACCESS_PUBLIC);
            }
            if (m_app == null) {
                Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);
                // get the event ID that is the last element in the Uri
                long eventID = Long.parseLong(uri.getLastPathSegment());
            } else {
                String where = CalendarContract.Events._ID + "=?";
                String[] args = new String[]{"" + m_app.getEventID()};
                int res = cr.update(CalendarContract.Events.CONTENT_URI, values, where, args);
                if (res == 1) {
                    getActivity().getContentResolver().notifyChange(CalendarContract.Events.CONTENT_URI, null);
                }
            }
            this.getActivity().finish();
        } else if (view.getId() == R.id.cancel) {
            if (m_app != null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Delete event and all recurrences?");
                ContentResolver cr = getActivity().getContentResolver();
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        ContentResolver cr = getActivity().getContentResolver();
                        Uri uri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, m_app.getEventID());
                        cr.delete(uri, null, null);
                        getActivity().getContentResolver().notifyChange(CalendarContract.Events.CONTENT_URI, null);
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.cancel();
                    }
                });
                Dialog d = builder.create();
                d.setOnDismissListener(new Dialog.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        getActivity().finish();
                    }
                });
                d.show();
            } else {
                this.getActivity().finish();
            }
        } else if (view.getId() == R.id.calendar_chooser) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Choose Calendar");
            String[] items = new String[m_CalendarNames.size()];
            this.m_CalendarNames.toArray(items);
            builder.setItems(items, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int pos) {
                    m_calendar_chooser.setBackgroundColor(m_CalendarColors.get(pos));
                    m_calendar_chooser.setText(m_CalendarNames.get(pos));
                    m_calendar_chooser.setTag(m_CalendarIds.get(pos));
                    m_tz = m_CalendarTimeZones.get(pos);
                    dialog.dismiss();
                }
            });
            Dialog d = builder.create();
            d.show();
        } else if (view.getId() == R.id.add_reminder) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Add Reminder");
            mPicker = new DiscordianNumberPicker(getActivity());
            builder.setView(mPicker);
            builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    int value = mPicker.getUnbiasedValue();
                    addReminder(value);
                    dialogInterface.dismiss();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            Dialog d = builder.create();
            d.show();
        }
    }

    public void addReminder(int value) {
        TextView t = (TextView) getActivity().getLayoutInflater().inflate(R.layout.reminder_item, null);
        t.setTag(new Integer(value));
        if (value > 0) {
            t.setText(value + " minutes before the event starts");
        } else if (value < 0) {
            t.setText(-value + " minutes after the event starts");
        } else {
            t.setText("at the exact moment the event starts");
        }
        t.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewEventFragment.this.onItemClick(view);
            }
        });
        m_reminder.addView(t);
    }

    @Override
    public void onTimezoneDialogClick(DialogFragment dialog, TimeZone tz) {
        m_tz = tz;
        m_timezone.setText(m_tz.getID() + " - " + m_tz.getDisplayName());
    }

    @Override
    public void onDateDialogClick(DateDialog dialog, DiscordianDay day, boolean begin) {
        if (begin) {
            m_begin_ddate = day;
            this.m_begin_date.setText(day.toString());
        } else {
            m_end_ddate = day;
            this.m_end_date.setText(day.toString());
        }
    }

    @Override
    public void onTimeDialogClick(TimeDialog dialog, int hour, int minute, boolean begin) {
        if (begin) {
            m_begin_time_hour = hour;
            m_begin_time_minute = minute;
            if (hour < 10) {
                if (minute < 10) {
                    m_begin_time.setText("0" + hour + ":0" + minute);
                } else {
                    m_begin_time.setText("0" + hour + ":" + minute);
                }
            } else {
                if (minute < 10) {
                    m_begin_time.setText(hour + ":0" + minute);
                } else {
                    m_begin_time.setText(hour + ":" + minute);
                }
            }
        } else {
            m_end_time_hour = hour;
            m_end_time_minute = minute;
            if (hour < 10) {
                if (minute < 10) {
                    m_end_time.setText("0" + hour + ":0" + minute);
                } else {
                    m_end_time.setText("0" + hour + ":" + minute);
                }
            } else {
                if (minute < 10) {
                    m_end_time.setText(hour + ":0" + minute);
                } else {
                    m_end_time.setText(hour + ":" + minute);
                }
            }
        }
    }

    public void onItemClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Remove Reminder?");
        builder.setMessage("Remove the reminder: " + ((TextView) view).getText() + "?");
        m_reminder.setTag(view);
        builder.setPositiveButton("Yes", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                m_reminder.removeView((View) m_reminder.getTag());
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        Dialog d = builder.create();
        d.show();
    }
}
