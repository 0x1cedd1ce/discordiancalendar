package li.keks.apps.discordiancalendar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RotateDrawable;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by ich on 04.03.14.
 */
public class ClockPickerView extends SurfaceView implements SurfaceHolder.Callback {

    private SurfaceHolder mSurfaceHolder;
    private SurfaceHolder mHolder;
    private Drawable mBackground;
    private Paint mBackgroundPaint;
    private Paint mActivePaint;
    private Paint mInactivePaint;
    private RotateDrawable mHour;
    private RotateDrawable mMinute;
    private ColorMatrixColorFilter mActiveColor;
    private ColorMatrixColorFilter mInactiveColor;
    private boolean mbHourSelected;
    private boolean mbMinuteSelected;
    private boolean mbPM;
    private int mTextSize = 0;
    private int mHeight = 0;
    private int mWidth = 0;

    public ClockPickerView(Context context) {
        super(context);
        mSurfaceHolder = this.getHolder();
        mSurfaceHolder.addCallback(this);
        ColorMatrix mat = new ColorMatrix();
        mat.setSaturation(1);
        mInactiveColor = new ColorMatrixColorFilter(mat);
        mat = new ColorMatrix();
        mat.setScale(0, 1, 0, 1);
        mActiveColor = new ColorMatrixColorFilter(mat);
    }

    public ClockPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mSurfaceHolder = this.getHolder();
        mSurfaceHolder.addCallback(this);
        ColorMatrix mat = new ColorMatrix();
        mat.setSaturation(1);
        mInactiveColor = new ColorMatrixColorFilter(mat);
        mat = new ColorMatrix();
        mat.setScale(0, 1, 0, 1);
        mActiveColor = new ColorMatrixColorFilter(mat);
    }

    public ClockPickerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mSurfaceHolder = this.getHolder();
        mSurfaceHolder.addCallback(this);
        ColorMatrix mat = new ColorMatrix();
        mat.setSaturation(1);
        mInactiveColor = new ColorMatrixColorFilter(mat);
        mat = new ColorMatrix();
        mat.setScale(0, 1, 0, 1);
        mActiveColor = new ColorMatrixColorFilter(mat);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            Pair<Double, Double> pos = CartToPol((int) event.getX(), (int) event.getY());
            int level = (int) (10000.0 * pos.second / (Math.PI * 2.0));
            //level = level - (level%(10000/60));
            if (Math.abs(level - mHour.getLevel()) < 500) {
                mHour.setColorFilter(mActiveColor);
                mMinute.setColorFilter(mInactiveColor);
                mbHourSelected = true;
                mbMinuteSelected = false;
            } else if (Math.abs(level - mMinute.getLevel()) < 500) {
                mHour.setColorFilter(mInactiveColor);
                mMinute.setColorFilter(mActiveColor);
                mbHourSelected = false;
                mbMinuteSelected = true;
            } else {
                mHour.setColorFilter(mInactiveColor);
                mMinute.setColorFilter(mInactiveColor);
                mbHourSelected = false;
                mbMinuteSelected = false;
            }
            redraw();
        } else if (event.getActionMasked() == MotionEvent.ACTION_UP) {
            //mHour.setColorFilter(mInactiveColor);
            //mMinute.setColorFilter(mInactiveColor);
        } else if (event.getActionMasked() == MotionEvent.ACTION_MOVE) {
            Pair<Double, Double> pos = CartToPol((int) event.getX(), (int) event.getY());
            int level = (int) (10000.0 * pos.second / (Math.PI * 2.0));
            if (mbHourSelected) {
                level = level - (level % (int) (9999.0 / 12.0));
                mHour.setLevel(level);
                getHour();
            }
            if (mbMinuteSelected) {
                level = level - (level % (int) (9999.0 / 60.0));
                mMinute.setLevel(level);
                getMinute();
            }
            redraw();
        }
        return true;
    }

    public void redraw() {
        Canvas canvas = mHolder.lockCanvas();
        canvas.drawColor(Color.WHITE);
        mBackground.draw(canvas);
        double radius = (double) mWidth / 2.0 - (double) mTextSize / 2.0 + 10;
        Pair<Integer, Integer> pos = PolToCart(radius, 0);
        canvas.drawText("3", pos.first, pos.second, mBackgroundPaint);
        pos = PolToCart(radius, Math.PI / 6.0);
        canvas.drawText("4", pos.first, pos.second, mBackgroundPaint);
        pos = PolToCart(radius, Math.PI / 3.0);
        canvas.drawText("5", pos.first, pos.second, mBackgroundPaint);
        pos = PolToCart(radius, Math.PI / 2.0);
        canvas.drawText("6", pos.first, pos.second, mBackgroundPaint);
        pos = PolToCart(radius, Math.PI * 2.0 / 3.0);
        canvas.drawText("7", pos.first, pos.second, mBackgroundPaint);
        pos = PolToCart(radius, Math.PI * 5.0 / 6.0);
        canvas.drawText("8", pos.first, pos.second, mBackgroundPaint);
        pos = PolToCart(radius, Math.PI);
        canvas.drawText("9", pos.first, pos.second, mBackgroundPaint);
        pos = PolToCart(radius, Math.PI * 7.0 / 6.0);
        canvas.drawText("10", pos.first, pos.second, mBackgroundPaint);
        pos = PolToCart(radius, Math.PI * 4.0 / 3.0);
        canvas.drawText("11", pos.first, pos.second, mBackgroundPaint);
        pos = PolToCart(radius, Math.PI * 3.0 / 2.0);
        canvas.drawText("12", pos.first, pos.second, mBackgroundPaint);
        pos = PolToCart(radius, Math.PI * 5.0 / 3.0);
        canvas.drawText("1", pos.first, pos.second, mBackgroundPaint);
        pos = PolToCart(radius, Math.PI * 11.0 / 6.0);
        canvas.drawText("2", pos.first, pos.second, mBackgroundPaint);

        if (mbPM) {
            canvas.drawText("AM", mWidth / 2 - 2 * mTextSize, mHeight / 2, mActivePaint);
            canvas.drawText("PM", mWidth / 2 + 2 * mTextSize, mHeight / 2, mInactivePaint);
        } else {
            canvas.drawText("AM", mWidth / 2 - 2 * mTextSize, mHeight / 2, mInactivePaint);
            canvas.drawText("PM", mWidth / 2 + 2 * mTextSize, mHeight / 2, mActivePaint);
        }

        mHour.draw(canvas);
        mMinute.draw(canvas);

        mHolder.unlockCanvasAndPost(canvas);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        mHolder = surfaceHolder;
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setColor(Color.BLACK);
        mActivePaint = new Paint();
        mActivePaint.setColor(Color.GREEN);
        mInactivePaint = new Paint();
        mInactivePaint.setColor(Color.DKGRAY);
        mBackground = getResources().getDrawable(R.drawable.circle);
        mHour = (RotateDrawable) getResources().getDrawable(R.drawable.hour);
        mMinute = (RotateDrawable) getResources().getDrawable(R.drawable.minute);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {
        mTextSize = width / 15;
        int l = mTextSize;
        int t = (height - width + mTextSize) / 2;
        int r = width - mTextSize;
        int b = t + r - l;
        mBackground.setBounds(l, t, r, b);
        mHour.setBounds(l, t, r, b);
        mMinute.setBounds(l, t, r, b);
        mWidth = width;
        mHeight = height;
        mBackgroundPaint.setTextSize(mTextSize);
        mBackgroundPaint.setTextAlign(Paint.Align.CENTER);
        mActivePaint.setTextSize(mTextSize);
        mInactivePaint.setTextSize(mTextSize);
        redraw();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
    }

    public Pair<Double, Double> CartToPol(int x, int y) {
        x = x - mWidth / 2;
        y = y - mHeight / 2;
        double radius = Math.sqrt(x * x + y * y);
        double arc = Math.atan2(y, x) + Math.PI;
        Pair<Double, Double> ret = new Pair<Double, Double>(radius, arc);
        return ret;
    }

    public Pair<Integer, Integer> PolToCart(double radius, double arc) {
        int x = (int) Math.round(Math.cos(arc) * radius) + mWidth / 2;
        int y = (int) Math.round(Math.sin(arc) * radius) + mHeight / 2;
        Pair<Integer, Integer> ret = new Pair<Integer, Integer>(x, y);
        return ret;
    }

    public int getHour() {
        int hour = ((int) Math.round((((double) mHour.getLevel() / 9999.0) * 12.0) + 9)) % 12;
        if (mbPM) {
            hour += 12;
        }
        //Log.i("getHour", hour+"");
        return hour;
    }

    public int getMinute() {
        int minute = ((int) Math.round((((double) mMinute.getLevel() / 9999.0) * 60.0) + 45)) % 60;
        //Log.i("getMinute", minute+"");
        return minute;
    }
}
